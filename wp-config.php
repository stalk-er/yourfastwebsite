<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'portfolio_website');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_HOME','http://yourfast.website.local');
define('WP_SITEURL','http://yourfast.website.local');


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '7l.^akjLYGGN<^ORF*-vn7OPE[yT:g|vcRPg Q3nL:|(MEi[&J:W# l?r3BFNfQ3');
define('SECURE_AUTH_KEY',  '_6lc0nI;cKEN=LvaM%7wZW;m5vW+B.!(9BO(tROhy61iz9vU@39bM!&Saia?zG]Y');
define('LOGGED_IN_KEY',    'z53Y R=7c,@t>/[HHqn]tX~8JS.Y?zPL3R]y3b5e(&>_jkAv;{7;=9/jPtjB1Hc}');
define('NONCE_KEY',        'rrh]|=,g-:c^2zVF.%XFP*~SzGY^*c-1CO)cJC2p*ItWj&*_qh|#!sVnjFZ?7p^(');
define('AUTH_SALT',        '-ni8[Rr$&MGl/YM&DgQ@EX|mIaV)#&z[n3y*DZb2M9uJoB=ZJm!DvL?lY;J,a_<n');
define('SECURE_AUTH_SALT', 'yV<0K[vOJr.z<YPGFFmuk0F,c-A!8Plg)Yw,R`=;10o6;gI9EdR[%lDII#4dQ_Yt');
define('LOGGED_IN_SALT',   '-Rb~)[s^0vV }<36VBSVJgw4vEAAi<b6 P{WLa.?bkzUWc5QKJRVb#At+eh!Z<,y');
define('NONCE_SALT',       ')mimL6e-DnTReoCawrYU3xo/p=[x`FHA0=a]c}$2b@K>1p2LFX |_e4:8,bs5*,=');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
