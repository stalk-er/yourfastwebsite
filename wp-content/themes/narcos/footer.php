			<?php 
				$tt_post_id = tt_get_page_id();
				$tt_check = get_post_meta( $tt_post_id, THEME_NAME . '_blog_shortcode', true );

				if(is_single('post')) {
					$tt_page_id = get_option( 'page_for_posts' );
				} elseif (tesla_has_woocommerce() && is_product()) {
					$tt_page_id = get_option( 'woocommerce_shop_page_id' );
				} else {
					$tt_page_id = tt_narcos_get_page_id();
				}

				$tt_shortcode = $tt_check ? get_post_meta( $tt_post_id, THEME_NAME . '_page_shortcode', true ) : get_post_meta( $tt_page_id, THEME_NAME . '_page_shortcode', true );
				echo do_shortcode($tt_shortcode);
			?>
			</div>

			<!-- Footer -->
			<footer>
				<?php $tt_social_platforms = array('facebook','twitter','google','pinterest','instagram','linkedin','dribbble','behance','youtube','flickr','rss');
                    $tt_show_platform = false;
                    foreach($tt_social_platforms as $platform) {
                        if (_go('show_' . $platform) || $tt_show_platform)
                            $tt_show_platform = true;
                        else
                            $tt_show_platform = false;
                    }
                if($tt_show_platform): ?>
				<div class="main-social-block">
					<!-- Rounded Border Bottom -->
					<div class="rounded-border-bottom">
						<span></span>
					</div>

					<div class="lazy-load">
						<div class="row">
							<div class="col-xs-24">
								<div class="social-block-group">
									<?php
										foreach($tt_social_platforms as $platform)
			                                if (_go('social_platforms_'. $platform) && _go('show_' . $platform)) 
			                            		printf('<a href="%s" target="_blank"><span>%s</span><i class="icon-%s"></i></a>', _go('social_platforms_'. $platform), $platform, $platform)
										;?>
								</div>
							</div>
						</div>
					</div>

					<!-- Rounded Border Bottom -->
					<div class="rounded-border-bottom reversed">
						<span></span>
					</div>

					<!-- Block Icon -->
					<i class="icon-share2 block-icon"></i>
				</div>
				<?php endif;?>

				<!-- Footer Wrapper -->
				<div class="main-footer-wrapper">
					<?php get_sidebar('footer');?>

					<div class="container copyrights align-center">
						<p>
							<?php if(_go('footer_info')): 
		                        _eo('footer_info');
		                    else:?>
		                        <?php esc_attr_e('Copyright ','narcos'); echo date('Y').' '; esc_attr_e('Designed and Developed by ','narcos');?><a href="<?php echo esc_url('https://www.teslathemes.com/'); ?>" target="_blank"><?php esc_attr_e('TeslaThemes','narcos'); ?></a>, <?php esc_attr_e('Supported by ', 'narcos');?><a href="<?php echo esc_url('https://wpmatic.io/'); ?>" target="_blank"><?php esc_attr_e('WPmatic','narcos');?></a>
		                    <?php endif;?>
						</p>
					</div>
				</div>
			</footer>
		</div>
	</div>

	<?php wp_footer(); ?>
</body>
</html>