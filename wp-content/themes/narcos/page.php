<?php get_header(); ?>	

<?php
/**
 * Page
 */
$tt_page_id = tt_narcos_get_page_id();
$tt_sidebar = get_post_meta( $tt_page_id, THEME_NAME . '_sidebar_position', true );
$tt_rev_slider = get_post_meta( $tt_page_id, THEME_NAME . '_rev_slider_alias', true );
$tt_sidebar = $tt_sidebar ? $tt_sidebar : 'right';
?>

<?php if(empty($tt_rev_slider))
	get_template_part('templates/intro','box');
else if($tt_rev_slider !== 'false'):?>
	<!-- Intro Box -->
	<div class="intro-box">
		<?php echo do_shortcode($tt_rev_slider);?>

		<!-- Scroll Down Btn -->
		<a href="#" class="scroll-btn">
			<span class="icon"></span>
		</a>

		<!-- Rounded Border Bottom -->
		<div class="rounded-border-bottom">
			<span></span>
		</div>

		<span class="after-rounded-bottom"></span>
	</div>
<?php endif;?>

<?php while ( have_posts() ) : the_post();?>
	<?php if(!has_shortcode(get_the_content(),'vc_row') || ($tt_sidebar!= 'full_width' && has_shortcode(get_the_content(),'vc_row'))): ?>
	<section class="section section-blog no-margin">
		<div class="row">
	<?php endif ?>	
			<?php if($tt_sidebar == "left"): ?>
			<div class="col-lg-8 col-lg-offset-2 col-md-9">
				<?php get_sidebar();?>
			</div>
			<?php endif;?>

			<?php if($tt_sidebar!= 'full_width'): ?>
			<div class="<?php print $tt_sidebar == "full_width" ? esc_attr('col-lg-12 col-lg-offset-6 col-md-18 col-md-offset-3') : esc_attr('col-lg-12 col-lg-offset-2 col-md-15');?>">
				<div class="single-post-content">
			<?php endif;?>

					<?php the_content();?>

			<?php if($tt_sidebar!= 'full_width'): ?>
				</div>
			</div>
			<?php endif;?>

			<?php if($tt_sidebar == "right"): ?>
			<div class="col-lg-8 col-lg-offset-2 col-md-9">
				<?php get_sidebar();?>
			</div>
			<?php endif;?>
	<?php if(!has_shortcode(get_the_content(),'vc_row') || ($tt_sidebar!= 'full_width' && has_shortcode(get_the_content(),'vc_row'))): ?>
		</div>
	</section>
	<?php endif;?>

	<?php comments_template();?>

<?php endwhile; ?>
<?php get_footer();?>