<?php
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';

vc_icon_element_fonts_enqueue( $type );

$icon = ${"icon_" . $type}; ?>
<div class="small-showcase-box <?php echo esc_attr($css_class.' '.$el_class);?>">
	<i class="icon <?php echo esc_attr($icon);?>" <?php print $icon_color ? 'style="color: '.$icon_color.'"' : '' ;?>></i>
	<h4 class="heading" <?php print $heading_color ? 'style="color: '.$heading_color.'"' : '' ;?>><?php print $title;?><span <?php print $heading_color ? 'style="color: '.$heading_color.'"' : '' ;?>><?php print $subtitle;?></span></h4>
	<?php print $content;?>
	<span class="box-footer" <?php print $icon_color ? 'style="color: '.$icon_color.'"' : '' ;?>>x</span>
</div>