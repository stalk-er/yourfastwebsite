<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';

$output = '<div class="counter-box '.$css_class.' '.$el_class.'" data-counter-value="'.$count_value.'" data-state="0"><span class="value-container"></span><p class="box-title">'.$count_title.'</p></div>';

print balanceTags($output);?>