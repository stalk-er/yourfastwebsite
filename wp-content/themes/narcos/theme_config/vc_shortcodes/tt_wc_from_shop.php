<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';

$args = array(
    'post_status'   =>  'publish',
    'post_type'     =>  'product',
);

if(!empty($offset_nr))
    $args['offset'] = $offset_nr;

if(!empty($category_slug))
    $args['product_cat'] = $category_slug;

if(!empty($nr_products))
    $args['posts_per_page'] = $nr_products;

if(!empty($order_by))
    $args['orderby'] = $order_by;

if(!empty($sort_order))
    $args['order'] = $sort_order;

$products = get_posts($args); ?>
<div class="widget widget_popular_products <?php echo esc_attr($css_class.' '.$el_class); ?>">
	<ul class="clean-list popular-products">
		<?php foreach($products as $product_item):
			global $product;
			$old_product = $product;
		    $product_id = $product_item->ID;
		    $product = wc_get_product($product_id);
		    $product_title = get_post_field('post_title', $product_id);
			$product_thumbnail = get_the_post_thumbnail($product_id, 'thumbnail');
		?>
	    <li class="popular-product">
			<div class="cover">
				<a href="<?php echo get_permalink($product_id) ?>">
					<?php print $product_thumbnail; ?>
				</a>
			</div>

			<h5 class="product-title">
				<a href="<?php echo get_permalink($product_id) ?>"><?php print $product_title;?></a>
			</h5>

			<span class="price"><?php print $product->get_price_html() ?></span>
		</li>
		<?php $product = $old_product; endforeach ?>
	</ul>
</div>