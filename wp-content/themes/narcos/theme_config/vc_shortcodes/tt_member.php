<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
$social_array = (array) vc_param_group_parse_atts( $socials );
?>

<div class="team-member-box <?php echo esc_attr($css_class.' '.$el_class);?>">
	<div class="box-cover">
		<div class="member-info-box align-center">
			<p><?php print $content;?></p>

			<ul class="clean-list social-block">
				<?php foreach( $social_array as $social_item ): ?>
					<li><a href="<?php echo esc_url($social_item['social_url']);?>"><i class="icon-<?php echo esc_attr($social_item['social_icon']);?>"></i></a></li>
				<?php endforeach; ?>
			</ul>
		</div>
		<img src="<?php echo wp_get_attachment_url($member_image);?>" alt="<?php echo esc_attr($member_name);?>" />
	</div>

	<div class="box-body align-center">
		<h4 class="name"><?php print $member_name;?></h4>
		<p class="position"><?php print $member_job;?></p>
	</div>
</div>
