<?php
$partenr_items = $css = $el_class = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );
$partner_items = (array) vc_param_group_parse_atts( $partner_items );
$css = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
?>

<div class="tt-carousel partners-carousel <?php echo esc_attr($css_class.' '.$el_class);?>" data-items-desktop="5" data-items-small-desktop="4" data-items-tablet="3" data-items-phone="2" data-infinite="true" data-dots="false" data-arrows="false" data-items-to-slide="1">
	<ul class="clean-list carousel-items">
		<?php foreach($partner_items as $partner_item): ?>
			<?php $partner_img = wp_get_attachment_image_src($partner_item['item_image'], 'original') ?>
			<li class="carousel-item">
				<div class="partner-box">
					<a href="<?php echo esc_url($partner_item['item_url']); ?>">
						<img src="<?php echo esc_url($partner_img[0]); ?>" alt="<?php esc_html_e('partner','narcos');?>">
					</a>
				</div>
			</li>
		<?php endforeach; ?>
	</ul>
</div>