<?php

$vc_is_wp_version_3_6_more = version_compare( preg_replace( '/^([\d\.]+)(\-.*$)/', '$1', get_bloginfo( 'version' ) ), '3.6' ) >= 0;

add_filter( 'vc_iconpicker-type-icomoon', 'tt_icon');

$colors_arr = array (
	esc_html__( 'Blue', 'narcos' ) => 'blue',
	esc_html__( 'Turquoise', 'narcos' ) => 'turquoise',
	esc_html__( 'Pink', 'narcos' ) => 'pink',
	esc_html__( 'Violet', 'narcos' ) => 'violet',
	esc_html__( 'Peacoc', 'narcos' ) => 'peacoc',
	esc_html__( 'Chino', 'narcos' ) => 'chino',
	esc_html__( 'Mulled Wine', 'narcos' ) => 'mulled_wine',
	esc_html__( 'Vista Blue', 'narcos' ) => 'vista_blue',
	esc_html__( 'Black', 'narcos' ) => 'black',
	esc_html__( 'Orange', 'narcos' ) => 'orange',
	esc_html__( 'Sky', 'narcos' ) => 'sky',
	esc_html__( 'Green', 'narcos' ) => 'green',
	esc_html__( 'Juicy pink', 'narcos' ) => 'juicy_pink',
	esc_html__( 'Sandy brown', 'narcos' ) => 'sandy_brown',
	esc_html__( 'Purple', 'narcos' ) => 'purple',
	esc_html__( 'White', 'narcos' ) => 'white',
);

$size_arr = array (
	esc_html__( 'Mini', 'narcos' ) => 'xs',
	esc_html__( 'Small', 'narcos' ) => 'sm',
	esc_html__( 'Normal', 'narcos' ) => 'md',
	esc_html__( 'Large', 'narcos' ) => 'lg'
);

function tt_icon($icons) {
	$icomoon = array(
		array('icon-media-clipvideofilm' => 'Video Film'),
		array('icon-interface-circlerighttrue' => 'Circle'),
		array('icon-interface-cmdcommand' => 'Apple Command'),
	);
	return array_merge($icons, $icomoon);
}

function tt_icons() {
	$icons = array(
			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'narcos' ),
				'value' => array(
					esc_html__( 'Font Awesome', 'narcos' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'narcos' ) => 'openiconic',
					esc_html__( 'Typicons', 'narcos' ) => 'typicons',
					esc_html__( 'Entypo', 'narcos' ) => 'entypo',
					esc_html__( 'Linecons', 'narcos' ) => 'linecons',
					esc_html__( 'Tesla Icons', 'narcos' ) => 'icomoon'
				),
				'admin_label' => false,
				'param_name' => 'type',
				'description' => esc_html__( 'Select icon library.', 'narcos' ),
			),

			array(
				'type' => 'dropdown',
				'heading' => esc_html__( 'Icon library', 'narcos' ),
				'value' => array(
					esc_html__( 'Font Awesome', 'narcos' ) => 'fontawesome',
					esc_html__( 'Open Iconic', 'narcos' ) => 'openiconic',
					esc_html__( 'Typicons', 'narcos' ) => 'typicons',
					esc_html__( 'Entypo', 'narcos' ) => 'entypo',
					esc_html__( 'Linecons', 'narcos' ) => 'linecons',
					esc_html__( 'Tesla Icons', 'narcos' ) => 'icomoon'
				),
				'admin_label' => false,
				'param_name' => 'type',
				'description' => esc_html__( 'Select icon library.', 'narcos' ),
				'dependency' => array(
					'element' => 'add_icon',
					'value' => 'true',
				),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'narcos' ),
				'param_name' => 'icon_fontawesome',
				'value' => 'fa fa-adjust', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false,
					// default true, display an "EMPTY" icon?
					'iconsPerPage' => 4000,
					// default 100, how many icons per/page to display, we use (big number) to display all icons in single page
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'fontawesome',
				),
				'description' => esc_html__( 'Select icon from library.', 'narcos' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'narcos' ),
				'param_name' => 'icon_openiconic',
				'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'type' => 'openiconic',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'openiconic',
				),
				'description' => esc_html__( 'Select icon from library.', 'narcos' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'narcos' ),
				'param_name' => 'icon_typicons',
				'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'type' => 'typicons',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'typicons',
				),
				'description' => esc_html__( 'Select icon from library.', 'narcos' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'narcos' ),
				'param_name' => 'icon_entypo',
				'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'type' => 'entypo',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'entypo',
				),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'narcos' ),
				'param_name' => 'icon_linecons',
				'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'type' => 'linecons',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'linecons',
				),
				'description' => esc_html__( 'Select icon from library.', 'narcos' ),
			),
			array(
				'type' => 'iconpicker',
				'heading' => esc_html__( 'Icon', 'narcos' ),
				'param_name' => 'icon_icomoon',
				'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
				'settings' => array(
					'emptyIcon' => false, // default true, display an "EMPTY" icon?
					'type' => 'icomoon',
					'iconsPerPage' => 4000, // default 100, how many icons per/page to display
				),
				'dependency' => array(
					'element' => 'type',
					'value' => 'icomoon',
				),
				'description' => esc_html__( 'Select icon from library.', 'narcos' ),
			));
	return $icons;
}
$tt_icons = tt_icons();

global $vc_add_css_animation;

$vc_add_css_animation = array(
	'type' => 'dropdown',
	'heading' => esc_html__( 'CSS Animation', 'narcos' ),
	'param_name' => 'css_animation',
	'admin_label' => true,
	'value' => array(
		esc_html__( 'No', 'narcos' ) => '',
		esc_html__( 'Top to bottom', 'narcos' ) => 'top-to-bottom',
		esc_html__( 'Bottom to top', 'narcos' ) => 'bottom-to-top',
		esc_html__( 'Left to right', 'narcos' ) => 'left-to-right',
		esc_html__( 'Right to left', 'narcos' ) => 'right-to-left',
		esc_html__( 'Appear from center', 'narcos' ) => 'appear'
	),
	'description' => esc_html__( 'Select type of animation for element to be animated when it "enters" the browsers viewport (Note: works only in modern browsers).', 'narcos' )
);

global $vc_column_width_list;
$vc_column_width_list = array(
	esc_html__('1 column - 1/12', 'narcos') => '1/12',
	esc_html__('2 columns - 1/6', 'narcos') => '1/6',
	esc_html__('3 columns - 1/4', 'narcos') => '1/4',
	esc_html__('4 columns - 1/3', 'narcos') => '1/3',
	esc_html__('5 columns - 5/12', 'narcos') => '5/12',
	esc_html__('6 columns - 1/2', 'narcos') => '1/2',
	esc_html__('7 columns - 7/12', 'narcos') => '7/12',
	esc_html__('8 columns - 2/3', 'narcos') => '2/3',
	esc_html__('9 columns - 3/4', 'narcos') => '3/4',
	esc_html__('10 columns - 5/6', 'narcos') => '5/6',
	esc_html__('11 columns - 11/12', 'narcos') => '11/12',
	esc_html__('12 columns - 1/1', 'narcos') => '1/1'
);

/* Custom Button
----------------------------------------------------------- */
vc_add_param( 'vc_btn', array(
	'type' => 'checkbox',
	'heading' => esc_html__( 'Use theme button?', 'narcos' ),
	'param_name' => 'theme_btn',
	'value' => array( esc_html__( 'Yes, please', 'narcos' ) => 'yes' ),
	'description' => esc_html__( 'Use template default button style', 'narcos' ),
));

/* Woocommerce
----------------------------------------------------------- */
vc_add_param( 'recent_products', array(
	'type' => 'checkbox',
	'heading' => esc_html__( 'Use simple style?', 'narcos' ),
	'param_name' => 'tt_widget_style',
	'value' => array( esc_html__( 'Yes, please', 'narcos' ) => 'yes' ),
));

/* Custom Heading element
----------------------------------------------------------- */
vc_map( array(
	'name' => esc_html__( 'Custom Heading', 'narcos' ),
	'base' => 'vc_custom_heading',
	'icon' => 'icon-wpb-ui-custom_heading',
	'show_settings_on_create' => true,
	'category' => esc_html__( 'Content', 'narcos' ),
	'description' => esc_html__( 'Text with Google fonts', 'narcos' ),
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Text source', 'narcos' ),
			'param_name' => 'source',
			'value' => array(
				esc_html__( 'Custom text', 'narcos' ) => '',
				esc_html__( 'Post or Page Title', 'narcos' ) => 'post_title'
			),
			'std' => '',
			'description' => esc_html__( 'Select text source.', 'narcos' )
		),
		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Text', 'narcos' ),
			'param_name' => 'text',
			'admin_label' => true,
			'value' => esc_html__( 'This is custom heading element', 'narcos' ),
			'description' => esc_html__( 'If you are using non-latin characters be sure to activate them under Settings/Visual Composer/General Settings.', 'narcos' ),					'description' => esc_html__( 'Note: If you are using non-latin characters be sure to activate them under Settings/Visual Composer/General Settings.', 'narcos' ),
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Make uppercase', 'narcos' ),
			'param_name' => 'make_uppercase',
			'value' => array( esc_html__( 'Yes, please', 'narcos' ) => 'yes' ),
			'description' => esc_html__( 'Transform text to uppercase', 'narcos' ),
		),
		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'No margin top', 'narcos' ),
			'param_name' => 'no_top',
			'value' => array( esc_html__( 'Yes, please', 'narcos' ) => 'yes' ),
			'description' => esc_html__( 'Best for section titles, remove top space.', 'narcos' ),
		),
		array(
			'type' => 'vc_link',
			'heading' => esc_html__( 'URL (Link)', 'narcos' ),
			'param_name' => 'link',
			'description' => esc_html__( 'Add link to custom heading.', 'narcos' ),
			// compatible with btn2 and converted from href{btn1}
		),
		array(
			'type' => 'font_container',
			'param_name' => 'font_container',
			'value' => '',
			'settings' => array(
				'fields' => array(
					'tag' => 'h2', // default value h2
					'text_align',
					'font_size',
					'line_height',
					'color',
					//'font_style_italic'
					//'font_style_bold'
					//'font_family'

					'tag_description' => esc_html__( 'Select element tag.', 'narcos' ),
					'text_align_description' => esc_html__( 'Select text alignment.', 'narcos' ),
					'font_size_description' => esc_html__( 'Enter font size.', 'narcos' ),
					'line_height_description' => esc_html__( 'Enter line height.', 'narcos' ),
					'color_description' => esc_html__( 'Select color for your element.', 'narcos' ),
					//'font_style_description' => esc_html__('Put your description here','narcos'),
					//'font_family_description' => esc_html__('Put your description here','narcos'),
				),
			),
			// 'description' => esc_html__( '', 'narcos' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Letter Spacing', 'narcos' ),
			'param_name' => 'letter_spacing',
			'description' => esc_html__( 'Enter letter spacing.', 'narcos' ),
		),

		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Use theme default font family?', 'narcos' ),
			'param_name' => 'use_theme_fonts',
			'value' => array( esc_html__( 'Yes', 'narcos' ) => 'yes' ),
			'description' => esc_html__( 'Use font family from the theme.', 'narcos' ),
		),

		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Select font weight', 'narcos' ),
			'param_name' => 'fnt_weight',
			'value' => array(
				'300',
				'400',
				'500',
				'700'
			),
			'std' => '300',
			'dependency' => array(
				'element' => 'use_theme_fonts',
				'value' => 'yes',
			),
		),


		array(
			'type' => 'google_fonts',
			'param_name' => 'google_fonts',
			'value' => 'font_family:Montserrat|font_style:400%20regular%3A400%3Anormal', // default
			//'font_family:'.rawurlencode('Abril Fatface:400').'|font_style:'.rawurlencode('400 regular:400:normal')
			// this will override 'settings'. 'font_family:'.rawurlencode('Exo:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic').'|font_style:'.rawurlencode('900 bold italic:900:italic'),
			'settings' => array(
				//'no_font_style' // Method 1: To disable font style
				//'no_font_style'=>true, // Method 2: To disable font style
				'fields' => array(
					//'font_family' => 'Abril Fatface:regular',
					//'Exo:100,100italic,200,200italic,300,300italic,regular,italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic',// Default font family and all available styles to fetch
					//'font_style' => '400 regular:400:normal',
					// Default font style. Name:weight:style, example: "800 bold regular:800:normal"
					'font_family_description' => esc_html__( 'Select font family.', 'narcos' ),
					'font_style_description' => esc_html__( 'Select font styling.', 'narcos' )
				)
			),
			'dependency' => array(
				'element' => 'use_theme_fonts',
				'value_not_equal_to' => 'yes',
			),
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'narcos' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'narcos' ),
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'narcos' ),
			'param_name' => 'css',
			// 'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'narcos' ),
			'group' => esc_html__( 'Design Options', 'narcos' )
		)
	),
) );


/* TT Team Member
----------------------------------------------------------- */
vc_map( array(
	'name' => esc_html__( 'Member', 'narcos' ),
	'base' => 'tt_member',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'narcos' ),
	'description' => esc_html__( 'Team Member', 'narcos' ),
	"params" => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Member Name', 'narcos' ),
			'param_name' => 'member_name',
			'description' => esc_html__( 'Provide a name for team member element', 'narcos' ),
			'admin_label' => true
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Member job location/position', 'narcos' ),
			'param_name' => 'member_job',
			'description' => esc_html__( 'Provide job position for this member', 'narcos' ),
			'admin_label' => true
		),

		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__( 'Text', 'narcos' ),
			'param_name' => 'content',
			'value' => esc_html__( '<p>I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>', 'narcos' ),
		),

		array(
			'type' => 'attach_image',
			'heading' => esc_html__( 'Member Cover', 'narcos' ),
			'param_name' => 'member_image',
			'value' => '',
			'description' => esc_html__( 'Select image from media library.', 'narcos' )
		),

		array(
			'type' => 'param_group',
			'heading' => esc_html__( 'Member social networks', 'narcos' ),
			'param_name' => 'socials',
			'description' => esc_html__( 'Provide social network accounts for team member', 'narcos' ),
			'value' => urlencode( json_encode( array(
				array(
					'social_icon' 	=> 'facebook',
					'social_url' 	=> '#'
				),
				array(
					'social_icon' 	=> 'twitter',
					'social_url' 	=> '#'
				),
			) ) ),
			'params' => array(
				array(
					'type' => 'dropdown',
					'heading' => esc_html__( 'Social network', 'narcos' ),
					'value' => array(
						esc_html__( 'Facebook', 'narcos' ) => 'facebook',
						esc_html__( 'Twitter', 'narcos' ) => 'twitter',
						esc_html__( 'Instagram', 'narcos' ) => 'instagram',
						esc_html__( 'Dribbble', 'narcos' ) => 'dribbble',
						esc_html__( 'LinkedIn', 'narcos' ) => 'linkedin'
					),
					'admin_label' => true,
					'param_name' => 'social_icon',
					'description' => esc_html__( 'Select social network', 'narcos' ),
				),
				array(
					'type' => 'textfield',
					'heading' => esc_html__( 'Social network url', 'narcos' ),
					'param_name' => 'social_url',
					'value' => '',
					'description' => esc_html__( 'Provide social network url (used target blank)', 'narcos' )
				)
			)
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'narcos' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'narcos' )
		)
	)
) );

/* Partners 
-----------------------------------------------------------------*/

vc_map( array(
    'name' => esc_attr__( 'Partners', 'narcos' ),
    'base' => 'tt_partners',
    'icon' => 'icon-wpb',
    'category' => esc_attr__( 'TeslaThemes', 'narcos' ),
    'description' => esc_attr__( 'Displays a list of partners logos.', 'narcos' ),
    "params" => array(
        array(
            'type' => 'param_group',
            'heading' => esc_attr__( 'Partner Item', 'narcos' ),
            'param_name' => 'partner_items',
            'description' => esc_attr__( 'Enter content for each partner (an image + an url).', 'narcos' ),
            'value' => urlencode( json_encode( array(
                array(
                    'item_image'        =>  '',
                    'item_url'      =>  '',
                )))),
            'params'    =>  array(
                array(
                    'type'  =>  'attach_image',
                    'heading'   =>  esc_attr__('Partner Logo','narcos'),
                    'param_name'    =>  'item_image',
                    'description'   =>  esc_attr__('Choose an image as a partner logo.','narcos'),
                    'admin_label' => true,
                ),
                array(
                    'type'  =>  'textfield',
                    'heading'   =>  esc_attr__('URL Link','narcos'),
                    'param_name'    =>  'item_url',
                    'description'   =>  esc_attr__('Paste an url link.','narcos'),
                ),
            )
        ),
        array(
            'type' => 'textfield',
            'heading' => esc_attr__( 'Extra class name', 'narcos' ),
            'param_name' => 'el_class',
            'description' => esc_attr__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'narcos' )
        ),
        array(
            'type' => 'css_editor',
            'heading' => esc_attr__( 'CSS box', 'narcos' ),
            'param_name' => 'css',
            'group' => esc_attr__( 'Design Options', 'narcos' )
        )        
        )
    ));


/* TT Custom Portfolio
----------------------------------------------------------- */
vc_map( array(
	'name' 		=> esc_html__( 'Portfolio Item', 'narcos' ),
	'base' 		=> 'tt_custom_portfolio',
	'category' => esc_html__( 'TeslaThemes', 'narcos' ),
	'description' => esc_html__( 'Tesla Portfolio Item', 'narcos' ),
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Title', 'narcos' ),
			'param_name' => 'title',
			'description' => esc_html__( 'Insert here portfolio item title.', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Category', 'narcos' ),
			'param_name' => 'category',
			'description' => esc_html__( 'Insert here portfolio item categories separated by comma.', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'vc_link',
			'heading' => esc_html__( 'URL (Link)', 'narcos' ),
			'param_name' => 'link',
			'description' => esc_html__( 'Add link to portfolio item.', 'narcos' ),
		),

		array(
			'type' => 'attach_image',
			'heading' => esc_html__( 'Cover', 'narcos' ),
			'param_name' => 'box_cover',
			'description' => esc_html__( 'Set cover for portfolio item', 'narcos' )
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'narcos' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'narcos' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'narcos' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design Options', 'narcos' )
		),
	)
) );

/* TT Portfolio
----------------------------------------------------------- */
$port_categories = get_terms( array(  'taxonomy' => 'tt_portfolio_tax', 'hide_empty' => true ) );
$port_cats['All Categories'] = '*';

if(!is_wp_error($port_categories)) {
	foreach($port_categories as $category) 
		$port_cats[$category->name] = $category->slug;
}

vc_map( array(
	'name' => esc_html__( 'Portfolio Feed', 'narcos' ),
	'base' => 'tt_portfolio',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'narcos' ),
	'description' => esc_html__( 'Tesla Portfolio Items', 'narcos' ),
	"params" => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Box Style', 'narcos' ),
			'value' => array(
				esc_html__( 'Style 1', 'narcos' ) => 'style-1',
				esc_html__( 'Style 2', 'narcos' ) => 'style-2',
			),
			'param_name' => 'box_style',
			'description' => esc_html__( 'Select style for portfolio item.', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Category', 'narcos' ),
			'value' => $port_cats,
			'param_name' => 'item_category',
			'description' => esc_html__( 'Select default category.', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Nr. of items', 'narcos' ),
			'param_name' => 'nr',
			'description' => esc_html__( 'Insert here number of items to show.', 'narcos' ),
			'admin_label' => true,
		),

		array(
            'type'  =>  'textfield',
            'heading'   =>  esc_html__('Offset nr','narcos'),
            'param_name'    =>  'offset',
            'description'   =>  esc_html__('Set a number of portfolio items to be offsetted. Default "0"','narcos'),
            'admin_label' => true,
        ),

		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Columns', 'narcos' ),
			'value' => array(
				esc_html__( '2 Columns', 'narcos' ) => 'col-md-12',
				esc_html__( '3 Columns', 'narcos' ) => 'col-sm-12 col-md-8',
				esc_html__( '4 Columns', 'narcos' ) => 'col-sm-12 col-md-6',
			),
			'param_name' => 'columns',
			'description' => esc_html__( 'Select column size.', 'narcos' ),
		),

		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Wide Layout?', 'narcos' ),
			'param_name' => 'wide_layout',
			'value' => array( esc_html__( 'Yes, please', 'narcos' ) => 'yes' ),
			'description' => esc_html__( 'Removes column padding.', 'narcos' ),
		),

		array(
			'type' => 'checkbox',
			'heading' => esc_html__( 'Hide Filters?', 'narcos' ),
			'param_name' => 'hide_filters',
			'value' => array( esc_html__( 'Yes, please', 'narcos' ) => 'yes' ),
			'description' => esc_html__( 'Hide portfolio filters.', 'narcos' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'narcos' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'narcos' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'narcos' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design Options', 'narcos' )
		)
	)
) );

/* TT Testimonials
----------------------------------------------------------- */
$testimonials_categories = get_terms( array( 'taxonomy' => 'testimonials_tax', 'hide_empty' => true ) );
$testimonials_cats['All'] = '';

if(!is_wp_error($testimonials_categories)) {
	foreach($testimonials_categories as $category) 
		$testimonials_cats[$category->name] = $category->slug;
}

vc_map( array(
	'name' => esc_html__( 'Testimonials', 'narcos' ),
	'base' => 'tt_testimonials',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'narcos' ),
	'description' => esc_html__( 'Tesla Testimonials Items', 'narcos' ),
	"params" => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Category', 'narcos' ),
			'value' => $testimonials_cats,
			'param_name' => 'category',
			'description' => esc_html__( 'Select default category.', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Nr. of items', 'narcos' ),
			'param_name' => 'nr',
			'description' => esc_html__( 'Insert here number of items to show.', 'narcos' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'narcos' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'narcos' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'narcos' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design Options', 'narcos' )
		)
	)
) );

/* TT Custom Heading
----------------------------------------------------------- */
vc_map( array(
	'name' => esc_html__( 'Section Title', 'narcos' ),
	'base' => 'tt_section_header',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'narcos' ),
	'description' => esc_html__( 'Tesla Custom Section Title', 'narcos' ),
	'params' => array(
		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Section Title', 'narcos' ),
			'param_name' => 'title',
			'admin_label' => true,
			'value' => esc_html__( 'This is custom heading title', 'narcos' ),
		),

		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Section Subtitle', 'narcos' ),
			'param_name' => 'sub_title',
			'admin_label' => true,
			'value' => esc_html__( 'This is custom heading subtitle', 'narcos' ),
		),

		array(
			'type' => 'font_container',
			'param_name' => 'font_container_title',
			'value' => '',
			'settings' => array(
				'fields' => array(
					'text_align' => 'center',
					'font_size',
					'line_height',
					'color',
					'text_align_description' => esc_html__( 'Select text alignment.', 'narcos' ),
					'font_size_description' => esc_html__( 'Enter font size.', 'narcos' ),
					'line_height_description' => esc_html__( 'Enter line height.', 'narcos' ),
					'color_description' => esc_html__( 'Select color for your element.', 'narcos' ),
				),
			),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'narcos' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'Style particular content element differently - add a existing class name (black, white, hero-section) or another custom class from CSS.', 'narcos' ),
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'Css', 'narcos' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design options', 'narcos' )
		)
	),
) );

/* TT Counter Box
----------------------------------------------------------- */
vc_map( array(
	'name' => esc_html__( 'Counter Box', 'narcos' ),
	'base' => 'tt_counter_box',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'narcos' ),
	'description' => esc_html__( 'Tesla Counter Box', 'narcos' ),
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Number', 'narcos' ),
			'param_name' => 'count_value',
			'description' => esc_html__( 'Insert counter box number', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Title', 'narcos' ),
			'param_name' => 'count_title',
			'description' => esc_html__( 'Insert counter box title', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'narcos' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'narcos' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'Css', 'narcos' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design options', 'narcos' )
		)
	),
) );

/* TT Video Box
----------------------------------------------------------- */
vc_map( array(
	'name' => esc_html__( 'Video Box', 'narcos' ),
	'base' => 'tt_video_box',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'narcos' ),
	'description' => esc_html__( 'Tesla Video Box', 'narcos' ),
	'params' => array(
		array(
			'type' => 'attach_image',
			'heading' => esc_html__( 'Background', 'narcos' ),
			'param_name' => 'background',
			'value' => '',
			'description' => esc_html__( 'Select background image for video box.', 'narcos' )
		),

		array(
			'type' => 'textarea_raw_html',
			'heading' => esc_html__( 'Embed Code', 'narcos' ),
			'param_name' => 'video_url',
			'description' => esc_html__( 'Insert here embed code', 'narcos' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'narcos' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'narcos' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'Css', 'narcos' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design options', 'narcos' )
		)
	),
) );

/* TT Contact Item
----------------------------------------------------------- */
vc_map( array(
	'name' 		=> esc_html__( 'Contact Item', 'narcos' ),
	'base' 		=> 'tt_contact_item',
	'category' => esc_html__( 'TeslaThemes', 'narcos' ),
	'description' => esc_html__( 'Tesla Contact Item', 'narcos' ),
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Title', 'narcos' ),
			'param_name' => 'title',
			'description' => esc_html__( 'Insert here item title.', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textarea_html',
			'heading' => esc_html__( 'Description', 'narcos' ),
			'param_name' => 'content',
			'description' => esc_html__( 'Insert here item description.', 'narcos' )
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'narcos' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'narcos' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'narcos' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design Options', 'narcos' )
		),
	)
) );

/* TT Info Box
----------------------------------------------------------- */
vc_map( array(
	'name' 		=> esc_html__( 'Info Box', 'narcos' ),
	'base' 		=> 'tt_shop_discount',
	'category' => esc_html__( 'TeslaThemes', 'narcos' ),
	'description' => esc_html__( 'Tesla Info Box', 'narcos' ),
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Title', 'narcos' ),
			'param_name' => 'title',
			'description' => esc_html__( 'Insert here box title.', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textarea',
			'heading' => esc_html__( 'Description', 'narcos' ),
			'param_name' => 'sub_title',
			'description' => esc_html__( 'Insert here box description.', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'vc_link',
			'heading' => esc_html__( 'URL (Link)', 'narcos' ),
			'param_name' => 'link',
			'description' => esc_html__( 'Add link to info box.', 'narcos' ),
		),

		array(
			'type' => 'attach_image',
			'heading' => esc_html__( 'Background', 'narcos' ),
			'param_name' => 'box_background',
			'description' => esc_html__( 'Set background for info box', 'narcos' )
		),

		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Style', 'narcos' ),
			'value' => array(
				esc_html__( 'Style 1', 'narcos' ) => 'style_1',
				esc_html__( 'Style 2', 'narcos' ) => 'style_2',
				esc_html__( 'Style 3', 'narcos' ) => 'style_3',
			),
			'param_name' => 'style',
			'description' => esc_html__( 'Select box style.', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'narcos' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'narcos' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'narcos' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design Options', 'narcos' )
		),
	)
) );

/* TT Woo Shop */
vc_map( array(
    'name' 		=> esc_html__( 'Woo From Shop', 'narcos' ),
    'base' 		=> 'tt_wc_from_shop',
    'category' => esc_html__( 'TeslaThemes', 'narcos' ),
    'description' => esc_html__( 'Create a section with products from shop', 'narcos' ),
    'params' => array(
        array(
            'type'  =>  'textfield',
            'heading'   =>  esc_html__('Category','narcos'),
            'param_name'    =>  'category_slug',
            'description'   =>  esc_html__('Category slug','narcos'),
            'admin_label' => true,
        ),
        array(
            'type'  =>  'textfield',
            'heading'   =>  esc_html__('Number of product','narcos'),
            'param_name'    =>  'nr_products',
            'description'   =>  esc_html__('Set a number of products to be displayed. Default "6"','narcos'),
            'value'         =>  '4',
            'admin_label' => true
        ),
        array(
            'type'  =>  'textfield',
            'heading'   =>  esc_html__('Offset nr','narcos'),
            'param_name'    =>  'offset_nr',
            'description'   =>  esc_html__('Set a number of products to be offsetted. Default "0"','narcos'),
            'admin_label' => true,
        ),

        array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Order by', 'narcos' ),
			'param_name' => 'order_by',
			'value' => array(
				'',
				esc_html__( 'Date', 'narcos' ) => 'date',
				esc_html__( 'ID', 'narcos' ) => 'ID',
				esc_html__( 'Author', 'narcos' ) => 'author',
				esc_html__( 'Title', 'narcos' ) => 'title',
				esc_html__( 'Modified', 'narcos' ) => 'modified',
				esc_html__( 'Random', 'narcos' ) => 'rand',
				esc_html__( 'Comment count', 'narcos' ) => 'comment_count',
				esc_html__( 'Menu order', 'narcos' ) => 'menu_order',
			),
			'description' => 'Select how to sort retrieved products.'
		),

		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Sort order', 'narcos' ),
			'param_name' => 'sort_order',
			'value' => array(
				'',
				esc_html__( 'Descending', 'narcos' ) => 'DESC',
				esc_html__( 'Ascending', 'narcos' ) => 'ASC',
			),
			'description' => 'Designates the ascending or descending order. '
		),

        array(
            'type' => 'textfield',
            'heading' => esc_html__( 'Extra class name', 'narcos' ),
            'param_name' => 'el_class',
            'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'narcos' )
        ),

        array(
            'type' => 'css_editor',
            'heading' => esc_html__( 'CSS box', 'narcos' ),
            'param_name' => 'css',
            'group' => esc_html__( 'Design Options', 'narcos' )
        )
    )
));

/* TT Blog Feed
----------------------------------------------------------- */
$post_categories = get_terms( array( 'taxonomy' => 'category', 'hide_empty' => true ) );
$post_cats['All Categories'] = '0';

if(!is_wp_error($post_categories)) {
	foreach($post_categories as $category) 
		$post_cats[$category->name] = $category->slug;
}

vc_map( array(
	'name' => esc_html__( 'Blog Feed', 'narcos' ),
	'base' => 'tt_blog_list',
	'icon' => 'icon-wpb',
	'category' => esc_html__( 'TeslaThemes', 'narcos' ),
	'description' => esc_html__( 'Tesla Blog Feed', 'narcos' ),
	'params' => array(
		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Category', 'narcos' ),
			'value' => $post_cats,
			'param_name' => 'tesla_category',
			'description' => esc_html__( 'Select from which category you want the posts to be displayed.', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Number of posts', 'narcos' ),
			'param_name' => 'nr_posts',
			'description' => esc_html__( 'Set a number of posts to be displayed. Default "2".', 'narcos' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Offset Nr', 'narcos' ),
			'param_name' => 'nr_posts_offset',
			'description' => esc_html__( 'Set a number of posts to be offsetted. Default "0".', 'narcos' ),
		),

		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Columns', 'narcos' ),
			'value' => array(
				esc_html__( '2 Columns', 'narcos' ) => 'col-md-12',
				esc_html__( '3 Columns', 'narcos' ) => 'col-md-8',
				esc_html__( '4 Columns', 'narcos' ) => 'col-md-6',
			),
			'param_name' => 'columns',
			'description' => esc_html__( 'Select column size.', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'dropdown',
			'heading' => esc_html__( 'Post Style', 'narcos' ),
			'value' => array(
				esc_html__( 'Style 1', 'narcos' ) => 'style_1',
				esc_html__( 'Style 2', 'narcos' ) => 'style_2',
			),
			'param_name' => 'post_style',
			'description' => esc_html__( 'Select post element style.', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Background Color', 'narcos' ),
			'param_name' => 'bg_color',
			'description' => esc_html__( 'Select custom background color for post element.', 'narcos' ),
			'dependency' => array(
				'element' => 'post_style',
				'value' => 'style_2',
			),
		),


		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'narcos' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'Style particular content element differently, add custom class from CSS.', 'narcos' ),
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'Css', 'narcos' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design options', 'narcos' )
		)
	),
) );

/* TT Folding Content
----------------------------------------------------------- */
vc_map( array(
	'name' 		=> esc_html__( 'Folding Content', 'narcos' ),
	'base' 		=> 'tt_folding_content',
	'category' => esc_html__( 'TeslaThemes', 'narcos' ),
	'description' => esc_html__( 'Tesla Folding Content', 'narcos' ),
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Left Box Title', 'narcos' ),
			'param_name' => 'left_title',
			'description' => esc_html__( 'Insert here left box title.', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Left Box Subtitle', 'narcos' ),
			'param_name' => 'left_subtitle',
			'description' => esc_html__( 'Insert here left box subtitle.', 'narcos' ),
		),

		array(
			'type' => 'attach_image',
			'heading' => esc_html__( 'Left Box Background', 'narcos' ),
			'param_name' => 'left_background',
			'description' => esc_html__( 'Set background for left box', 'narcos' )
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Right Box Title', 'narcos' ),
			'param_name' => 'right_title',
			'description' => esc_html__( 'Insert here right box title.', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Right Box Subtitle', 'narcos' ),
			'param_name' => 'right_subtitle',
			'description' => esc_html__( 'Insert here right box subtitle.', 'narcos' ),
		),

		array(
			'type' => 'attach_image',
			'heading' => esc_html__( 'Right Box Background', 'narcos' ),
			'param_name' => 'right_background',
			'description' => esc_html__( 'Set background for right box', 'narcos' )
		),

		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__( 'Text', 'narcos' ),
			'param_name' => 'content',
			'value' => esc_html__( '<p>I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>', 'narcos' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'narcos' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'narcos' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'narcos' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design Options', 'narcos' )
		),
	)
) );

/* TT Image Showcase
----------------------------------------------------------- */
vc_map( array(
	'name' 		=> esc_html__( 'Image Showcase', 'narcos' ),
	'base' 		=> 'tt_image_showcase',
	'category' => esc_html__( 'TeslaThemes', 'narcos' ),
	'description' => esc_html__( 'Tesla Image Showcase', 'narcos' ),
	'params' => array(
		array(
			'type' => 'attach_image',
			'heading' => esc_html__( 'Image', 'narcos' ),
			'param_name' => 'showcase_img',
			'description' => esc_html__( 'Set showcase image', 'narcos' )
		),

		array(
			'type' => 'attach_image',
			'heading' => esc_html__( 'Background', 'narcos' ),
			'param_name' => 'showcase_bg',
			'description' => esc_html__( 'Set showcase background image', 'narcos' )
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'CSS Settings', 'narcos' ),
			'param_name' => 'showcase_settings',
			'description' => esc_html__( 'Insert here inline-css, if showcase img will not fit in showcase background', 'narcos' )
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'narcos' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'narcos' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'narcos' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design Options', 'narcos' )
		),
	)
) );

/* TT Image Showcase
----------------------------------------------------------- */
vc_map( array(
	'name' 		=> esc_html__( 'Feature Box', 'narcos' ),
	'base' 		=> 'tt_feature',
	'category' => esc_html__( 'TeslaThemes', 'narcos' ),
	'description' => esc_html__( 'Tesla Feature Box', 'narcos' ),
	'params' => array(

		$tt_icons[0],
		$tt_icons[2],
		$tt_icons[3],
		$tt_icons[4],
		$tt_icons[5],
		$tt_icons[6],
		$tt_icons[7],

		array(
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Icon Color', 'narcos' ),
			'param_name' => 'icon_color',
			'description' => esc_html__( 'Select custom color for icons.', 'narcos' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Title', 'narcos' ),
			'param_name' => 'title',
			'description' => esc_html__( 'Insert here item title', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Subtitle', 'narcos' ),
			'param_name' => 'subtitle',
			'description' => esc_html__( 'Insert here item subtitle', 'narcos' ),
			'admin_label' => true,
		),

		array(
			'type' => 'colorpicker',
			'heading' => esc_html__( 'Heading color', 'narcos' ),
			'param_name' => 'heading_color',
			'description' => esc_html__( 'Select custom color for title & subtitle.', 'narcos' ),
		),

		array(
			'type' => 'textarea_html',
			'holder' => 'div',
			'heading' => esc_html__( 'Text', 'narcos' ),
			'param_name' => 'content',
			'value' => esc_html__( '<p>I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>', 'narcos' ),
		),

		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'narcos' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'narcos' )
		),

		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'narcos' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design Options', 'narcos' )
		),
	)
) );

/* TT Google Map
----------------------------------------------------------- */
vc_map( array(
	'name' 		=> esc_html__( 'Google Map', 'narcos' ),
	'base' 		=> 'tt_gmap',
	'category' => esc_html__( 'TeslaThemes', 'narcos' ),
	'description' => esc_html__( 'Create Google Map', 'narcos' ),
	'params' => array(
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Map height', 'narcos' ),
			'param_name' => 'map_height',
			'value' => '',
			'description' => esc_html__( 'Set map height. Please provide value with unit of measurement (px, cm, in)'
				, 'narcos' ),
		),
		array(
			'type' => 'attach_image',
			'heading' => esc_html__( 'Map Pin', 'narcos' ),
			'param_name' => 'map_pin',
			'value' => '', // default video url
			'description' => esc_html__( 'Set pin for map', 'narcos' )
		),
		array(
			'type' => 'textfield',
			'heading' => esc_html__( 'Extra class name', 'narcos' ),
			'param_name' => 'el_class',
			'description' => esc_html__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'narcos' )
		),
		array(
			'type' => 'google_map',
			'heading' => esc_html__( 'Google Map Editor', 'narcos' ),
			'param_name' => 'map_settings',
			'group' => esc_html__( 'Map Editor', 'narcos' )
		),
		array(
			'type' => 'css_editor',
			'heading' => esc_html__( 'CSS box', 'narcos' ),
			'param_name' => 'css',
			'group' => esc_html__( 'Design Options', 'narcos' )
		)
	)
) );