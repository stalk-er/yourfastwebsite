<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
$url = vc_build_link( $link );
$image_url = wp_get_attachment_url( $box_cover );
$cats = explode(",", $category);

$output = '<div class="portfolio-box"><div class="box-cover"><div class="box-info"><h4 class="portfolio-item-title">'.$title.'</h4>';
$output .= '<ul class="clean-list portfolio-item-categories">';

if(!empty($cats))
	foreach($cats as $cat)
	$output .= '<li>'.$cat.'</li>';

$output .= '</ul></div>';
$output .= $url['url'] ? '<a href="'.esc_url($url['url']).'" title="'.esc_attr($url['title']).'" target="'.( strlen( $url['target'] ) > 0 ? esc_attr( $url['target'] ) : '_self').'">' : '';
$output .= '<img src="'.$image_url.'" alt="'.$title.'">';
$output .= $url['url'] ? '</a>' : '';
$output .= '</div></div>';

print balanceTags($output);?>