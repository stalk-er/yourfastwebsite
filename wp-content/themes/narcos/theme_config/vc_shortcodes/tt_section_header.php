<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$d_title = $this->getStyles($font_container_title);
$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';

$output = '<div class="section-title align-center '.$css_class.' '.$el_class.'">';
$output .= '<h2 style="'.$d_title.'">'.$title.'<span>'.$sub_title.'</span></h2>';
$output .= '</div>';

print balanceTags($output);
?>