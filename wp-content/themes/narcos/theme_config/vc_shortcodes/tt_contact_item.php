<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';

$output = '<div class="contact-block '.$css_class.' '.$el_class.'"><h5 class="block-title">'.$title.'</h5>'.$content.'</div>';

print balanceTags($output);?>