<?php
/* TT Custom Heading */
class WPBakeryShortCode_TT_section_header extends WPBakeryShortCode {
	public function getStyles( $font_container_data ) {
		$styles = array();

		if (!empty($font_container_data)) {
			$pieces = explode("|", $font_container_data);

			foreach($pieces as $piece) 
				$styles[] = str_replace(array('_', '%23'), array('-', '#'), $piece).';';			
		}
		return implode('',$styles);
	}
}

/* TT Counter Box */
class WPBakeryShortCode_TT_counter_box extends WPBakeryShortCode {}

/* TT Member */
class WPBakeryShortCode_TT_member extends WPBakeryShortCode {}

/* TT Video Box */
class WPBakeryShortCode_TT_video_box extends WPBakeryShortCode {}

/* TT Contact Item */
class WPBakeryShortCode_TT_contact_item extends WPBakeryShortCode {}

/* TT Shop Promo */
class WPBakeryShortCode_TT_shop_discount extends WPBakeryShortCode {}

/* TT Woo Shop */
class WPBakeryShortCode_TT_wc_from_shop extends WPBakeryShortCode {}

/* TT Custom Portfolio */
class WPBakeryShortCode_TT_custom_portfolio extends WPBakeryShortCode {}

/* TT Partners List */
class WPBakeryShortCode_TT_partners extends WPBakeryShortCode {
}

/* TT Blog Feed */
class WPBakeryShortCode_TT_blog_list extends WPBakeryShortCode {
}

/* TT Folding Content */
class WPBakeryShortCode_TT_folding_content extends WPBakeryShortCode {
}

/* TT Image Showcase */
class WPBakeryShortCode_TT_image_showcase extends WPBakeryShortCode {
}

/* TT Feature Box */
class WPBakeryShortCode_TT_feature extends WPBakeryShortCode {
}

/* TT Google Map */
class WPBakeryShortCode_TT_GMap extends WPBakeryShortCode {
}