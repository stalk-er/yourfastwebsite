<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : ''; ?>

<div class="folding-content <?php echo esc_attr($el_class.' '.$css);?>">
	<div class="row row-fit">
		<div class="col-md-12">
			<div class="folding-item folding-item-text left">
				<a href="#">
					<div class="item-text">
						<h2><?php print $left_title;?><span><?php print $left_subtitle;?></span></h2>
					</div>
					<img src="<?php echo wp_get_attachment_url( $left_background );?>" alt="<?php echo esc_attr($left_title);?>">
				</a>
			</div>
		</div>

		<div class="col-md-12">
			<div class="folding-item folding-item-text right">
				<a href="#">
					<div class="item-text">
						<h2><?php print $right_title;?><span><?php print $right_subtitle;?></span></h2>
					</div>
					<img src="<?php echo wp_get_attachment_url( $right_background );?>" alt="<?php echo esc_attr($right_title);?>">
				</a>
			</div>
		</div>
	</div>

	<!-- Content Panel -->
	<div class="content-panel">
		<!-- Close Content Panel -->
		<span class="close-content-toggle"></span>

		<span class="bg-panel left"></span>
		<span class="bg-panel right"></span>

		<div class="innner-content-panel">
			<?php print $content;?>
		</div>
	</div>
</div>