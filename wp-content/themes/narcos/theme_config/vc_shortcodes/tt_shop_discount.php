<?php 
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

$css_class = !empty($css) ? vc_shortcode_custom_css_class( $css ) : '';
$url = vc_build_link( $link );
$image_url = wp_get_attachment_url( $box_background );

if($style == 'style_1') {
	$output = '<div class="discount-box '.$css_class.' '.$el_class.'"><div class="box-text"><h5 class="box-title">'.$title.'</h5><p>'.$sub_title.'</p></div>';
	$output .= '<div class="cover">';
	$output .= $url['url'] ? '<a href="'.esc_url($url['url']).'" title="'.esc_attr($url['title']).'" target="'.( strlen( $url['target'] ) > 0 ? esc_attr( $url['target'] ) : '_self').'">' : '';
	$output .= '<img src="'.$image_url.'" alt="'.$title.'">';
	$output .= $url['url'] ? '</a>' : '';
	$output .= '</div>';
	$output .= '</div>';
} elseif($style == 'style_2') {
	$output = '<div class="folding-item">';
	$output .= $url['url'] ? '<a href="'.esc_url($url['url']).'" title="'.esc_attr($url['title']).'" target="'.( strlen( $url['target'] ) > 0 ? esc_attr( $url['target'] ) : '_self').'">' : '';
	$output .= '<div class="item-text"><h2>'.$title.'<span>'.$sub_title.'</span></h2></div>';
	$output .= '<img src="'.$image_url.'" alt="'.$title.'">';
	$output .= $url['url'] ? '</a>' : '';
	$output .= '</div>';		
} else {
	$output = '<div class="about-block"><div class="block-cover"><img src="'.$image_url.'" alt="'.$title.'"></div>';
	$output .= '<div class="block-body align-center"><h4 class="block-title">';
	$output .= $url['url'] ? '<a href="'.esc_url($url['url']).'" title="'.esc_attr($url['title']).'" target="'.( strlen( $url['target'] ) > 0 ? esc_attr( $url['target'] ) : '_self').'">' : '';
	$output .= $title;
	$output .= $url['url'] ? '</a>' : '';
	$output .= '</h4><p>'.$sub_title.'</p>';
	$output .= '</div></div>';			
}

print balanceTags($output);?>