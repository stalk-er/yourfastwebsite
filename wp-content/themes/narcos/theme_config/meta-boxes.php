<?php 

$tt_logo_pos = get_post_meta(get_option( 'page_for_posts' ), THEME_NAME.'_logo_pos', true);
$tt_menu_pos = get_post_meta(get_option( 'page_for_posts' ), THEME_NAME.'_menu_style', true);

return array(
	'metaboxes'	=>	array(
		array(
			'id'             => 'page_metabox',            // meta box id, unique per meta box
			'title'          => esc_html_x('Page Settings','meta boxes','narcos'),   // meta box title
			'post_type'      => array('page'),		// post types, accept custom post types as well, default is array('post'); optional
			'taxonomy'       => array(),    // taxonomy name, accept categories, post_tag and custom taxonomies
			'context'		 => 'normal',						// where the meta box appear: normal (default), advanced, side; optional
			'priority'		 => 'low',						// order of meta box: high (default), low; optional
			'input_fields'   => array(
				'rev_slider_alias'=>array(
                    'name'	=>	esc_html_x("Insert Revolution Slider alias shortcode for page header. ",'meta boxes','narcos'),
                    'type'	=>	"text",
                ),

                'sidebar_position'  =>  array(
					'name'  =>  esc_html_x('Sidebar Position','meta boxes','narcos'),
					'type'  =>  'select',
					'values'    =>  array(
							'full_width'    =>  esc_html_x('No Sidebar/Full Width','meta boxes','narcos'),
							'right'         =>  esc_html_x('Right','meta boxes','narcos'),
							'left'          =>  esc_html_x('Left','meta boxes','narcos'),
						),
					'std'   =>  'full_width'  //default value selected
				),

				'header_type'=>array(
					'name'=> esc_html_x('Header Type','meta boxes','narcos'),
					'type'=>'select',
					'values'=>array(
							'sticky-header'=>esc_html_x('Sticky','meta boxes','narcos'),
							'no-sticky'=>esc_html_x('No Sticky','meta boxes','narcos'),
					),
				'std'=>'no-sticky'  //default value selected
				),

				'logo_pos'  =>  array(
					'name'  =>  esc_html_x('Logo Position','meta boxes','narcos'),
					'type'  =>  'select',
					'values'    =>  array(
                        'logo-left'   =>  esc_html_x('Left','meta boxes','narcos'),
						'logo-center'   =>  esc_html_x('Center','meta boxes','narcos'),
						'logo-right'   =>  esc_html_x('Right','meta boxes','narcos'),
					),
					'std'   =>  _go('logo_position') ? _go('logo_position') : 'logo-left'
				),

				'menu_style'  =>  array(
					'name'  =>  esc_html_x('Menu Style','meta boxes','narcos'),
					'type'  =>  'select',
					'values'    =>  array(
                        'expanded'   =>  esc_html_x('Expanded','meta boxes','narcos'),
						'boxed'   =>  esc_html_x('Boxed','meta boxes','narcos'),
					),
					'std'   =>  _go('menu_style') ? _go('menu_style') : 'expanded'
				),  
				

				'page_background'=>array(
		    		'name'=>'Header Background',
		    		'type'=>'image',
		    		'desc'=>'If you want a different background to be used for this page, then you can upload it here.'
		    	),

		    	'page_shortcode'=>array(
		    		'name'=>'Custom Shortcode',
		    		'type'=>'textarea',
		    		'desc'=>'Insert custom shortcodes to use them in page footer.'
	    		),
			)
		),

		array(
			'id'             => 'post_metabox',            // meta box id, unique per meta box
			'title'          => esc_html_x('Post Settings','meta boxes','narcos'),   // meta box title
			'post_type'      => array('post'),		// post types, accept custom post types as well, default is array('post'); optional
			'taxonomy'       => array(),    // taxonomy name, accept categories, post_tag and custom taxonomies
			'context'		 => 'normal',						// where the meta box appear: normal (default), advanced, side; optional
			'priority'		 => 'low',						// order of meta box: high (default), low; optional
			'input_fields'   => array(
				'video_url'=>array(
		    		'name'=> esc_html_x('Video URL','meta boxes','narcos'),
		    		'type'=>'text',
		    		'desc'=>'Insert video url for video post type.'
				),

				'audio_url'=>array(
		    		'name'=> esc_html_x('Audio URL','meta boxes','narcos'),
		    		'type'=>'text',
		    		'desc'=>'Insert audio url for audio post type.'
				),

				'sidebar_position'  =>  array(
					'name'  =>  esc_html_x('Sidebar Position','meta boxes','narcos'),
					'type'  =>  'select',
					'values'    =>  array(
							'as_blog'   	=>  esc_html_x('Same as Blog Page','meta boxes','narcos'),
							'full_width'    =>  esc_html_x('No Sidebar/Full Width','meta boxes','narcos'),
							'right'         =>  esc_html_x('Right','meta boxes','narcos'),
							'left'          =>  esc_html_x('Left','meta boxes','narcos'),
						),
					'std'   =>  'right'  //default value selected
				),

				'header_type'=>array(
					'name'=> esc_html_x('Header Type','meta boxes','narcos'),
					'type'=>'select',
					'values'=>array(
							'sticky-header'=>esc_html_x('Sticky','meta boxes','narcos'),
							'no-sticky'=>esc_html_x('No Sticky','meta boxes','narcos'),
					),
				'std'=> _go('header_sticky') ? _go('header_sticky') : 'no-sticky'  //default value selected
				),

				'logo_pos'  =>  array(
					'name'  =>  esc_html_x('Logo Position','meta boxes','narcos'),
					'type'  =>  'select',
					'values'    =>  array(
                        'logo-left'   =>  esc_html_x('Left','meta boxes','narcos'),
						'logo-center'   =>  esc_html_x('Center','meta boxes','narcos'),
						'logo-right'   =>  esc_html_x('Right','meta boxes','narcos'),
					),
					'std'   =>  $tt_logo_pos ? $tt_logo_pos : (_go('logo_position') ? _go('logo_position') : 'logo-left')
				),

				'menu_style'  =>  array(
					'name'  =>  esc_html_x('Menu Style','meta boxes','narcos'),
					'type'  =>  'select',
					'values'    =>  array(
                        'expanded'   =>  esc_html_x('Expanded','meta boxes','narcos'),
						'boxed'   =>  esc_html_x('Boxed','meta boxes','narcos'),
					),
					'std'   =>  $tt_menu_pos ? $tt_menu_pos : (_go('menu_style') ? _go('menu_style') : 'expanded')
				),

	    		'blog_shortcode'=>array(
		    		'name' => 'Use Custom Shortcode?',
		    		'type' => 'checkbox',
		    		'desc'=>'Check if you want to use custom shortcode for this post, other than shortcode from blog page'
	    		),

	    		'page_shortcode'=>array(
		    		'name'=>'Custom Shortcode',
		    		'type'=>'textarea',
		    		'desc'=>'Insert custom shortcodes to use them in post footer.'
	    		),
			)
		),

		array(
			'id'             => 'portfolio_metabox',            // meta box id, unique per meta box
			'title'          => esc_html_x('Portfolio Settings','meta boxes','narcos'),   // meta box title
			'post_type'      => array('tt_portfolio'),		// post types, accept custom post types as well, default is array('post'); optional
			'taxonomy'       => array(),    // taxonomy name, accept categories, post_tag and custom taxonomies
			'context'		 => 'normal',						// where the meta box appear: normal (default), advanced, side; optional
			'priority'		 => 'low',						// order of meta box: high (default), low; optional
			'input_fields'   => array(
				'sidebar_position'  =>  array(
					'name'  =>  esc_html_x('Sidebar Position','meta boxes','narcos'),
					'type'  =>  'select',
					'values'    =>  array(
							'full_width'    =>  esc_html_x('No Sidebar/Full Width','meta boxes','narcos'),
							'right'         =>  esc_html_x('Right','meta boxes','narcos'),
							'left'          =>  esc_html_x('Left','meta boxes','narcos'),
						),
					'std'   =>  'full_width'  //default value selected
				),

				'header_type'=>array(
					'name'=> esc_html_x('Header Type','meta boxes','narcos'),
					'type'=>'select',
					'values'=>array(
							'sticky-header'=>esc_html_x('Sticky','meta boxes','narcos'),
							'no-sticky'=>esc_html_x('No Sticky','meta boxes','narcos'),
					),
				'std'=> _go('header_sticky') ? _go('header_sticky') : 'no-sticky'  //default value selected
				),

				'logo_pos'  =>  array(
					'name'  =>  esc_html_x('Logo Position','meta boxes','narcos'),
					'type'  =>  'select',
					'values'    =>  array(
                        'logo-left'   =>  esc_html_x('Left','meta boxes','narcos'),
						'logo-center'   =>  esc_html_x('Center','meta boxes','narcos'),
						'logo-right'   =>  esc_html_x('Right','meta boxes','narcos'),
					),
					'std'   =>  _go('logo_position') ? _go('logo_position') : 'logo-left'
				),

				'menu_style'  =>  array(
					'name'  =>  esc_html_x('Menu Style','meta boxes','narcos'),
					'type'  =>  'select',
					'values'    =>  array(
                        'expanded'   =>  esc_html_x('Expanded','meta boxes','narcos'),
						'boxed'   =>  esc_html_x('Boxed','meta boxes','narcos'),
					),
					'std'   =>  _go('menu_style') ? _go('menu_style') : 'expanded'
				),

				'page_background'=>array(
		    		'name'=>'Header Background',
		    		'type'=>'image',
		    		'desc'=>'If you want a different background to be used for this page, then you can upload it here.'
		    	),

				'page_shortcode'=>array(
		    		'name'=>'Custom Shortcode',
		    		'type'=>'textarea',
		    		'desc'=>'Insert custom shortcodes to use them in page footer.'
	    		),
			)
		),

		array(
			'id'             => 'product_metabox',            // meta box id, unique per meta box
			'title'          => esc_html_x('Product Settings','meta boxes','narcos'),   // meta box title
			'post_type'      => array('product'),		// post types, accept custom post types as well, default is array('post'); optional
			'taxonomy'       => array(),    // taxonomy name, accept categories, post_tag and custom taxonomies
			'context'		 => 'normal',						// where the meta box appear: normal (default), advanced, side; optional
			'priority'		 => 'low',						// order of meta box: high (default), low; optional
			'input_fields'   => array(
				'rev_slider_alias'=>array(
                    'name'	=>	esc_html_x("Insert Revolution Slider alias shortcode for product header. ",'meta boxes','narcos'),
                    'type'	=>	"text",
                ),

				'logo_pos'  =>  array(
					'name'  =>  esc_html_x('Logo Position','meta boxes','narcos'),
					'type'  =>  'select',
					'values'    =>  array(
                        'logo-left'   =>  esc_html_x('Left','meta boxes','narcos'),
						'logo-center'   =>  esc_html_x('Center','meta boxes','narcos'),
						'logo-right'   =>  esc_html_x('Right','meta boxes','narcos'),
					),
					'std'   =>  _go('logo_position') ? _go('logo_position') : 'logo-left'
				),

				'header_type'=>array(
					'name'=> esc_html_x('Header Type','meta boxes','narcos'),
					'type'=>'select',
					'values'=>array(
							'sticky-header'=>esc_html_x('Sticky','meta boxes','narcos'),
							'no-sticky'=>esc_html_x('No Sticky','meta boxes','narcos'),
					),
				'std'=> _go('header_sticky') ? _go('header_sticky') : 'no-sticky'  //default value selected
				),

				'menu_style'  =>  array(
					'name'  =>  esc_html_x('Menu Style','meta boxes','narcos'),
					'type'  =>  'select',
					'values'    =>  array(
                        'expanded'   =>  esc_html_x('Expanded','meta boxes','narcos'),
						'boxed'   =>  esc_html_x('Boxed','meta boxes','narcos'),
					),
					'std'   =>  _go('menu_style') ? _go('menu_style') : 'expanded'
				),  
				

				'page_background'=>array(
		    		'name'=>'Header Background',
		    		'type'=>'image',
		    		'desc'=>'If you want a different background to be used for this page, then you can upload it here.'
		    	),

		    	'blog_shortcode'=>array(
		    		'name' => 'Use Custom Shortcode?',
		    		'type' => 'checkbox',
		    		'desc'=>'Check if you want to use custom shortcode for this product, other than shortcode from shop page'
	    		),

	    		'page_shortcode'=>array(
		    		'name'=>'Custom Shortcode',
		    		'type'=>'textarea',
		    		'desc'=>'Insert custom shortcodes to use them in product footer.'
	    		),
			)
		),

	)
);