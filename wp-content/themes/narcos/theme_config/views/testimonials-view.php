<?php 
$el_class = $shortcode['el_class'];
$css_class = !empty($shortcode['css']) ? vc_shortcode_custom_css_class( $shortcode['css'] ) : '';
?>

<div class="slider tt-slider testimonials-slider <?php echo esc_attr($css_class.' '.$el_class);?>" data-start="1" data-fade="false" data-speed="750" data-dots="true" data-arrows="false" data-infinite="true">
	<ul class="clean-list slides-list">
		<?php foreach ($slides as $slide_nr => $slide) : if($slide_nr >= $shortcode['nr']) break; ?>
		<li class="slide">
			<div class="container">
				<div class="testimonials-box">
					<p class="message"><?php print $slide['options']['testimonial_text'];?></p>

					<div class="testimonial-client">
						<h4 class="name"><?php echo get_the_title($slide['post']->ID);?></h4>
						<p class="job"><?php print $slide['options']['testimonial_position'];?></p>
					</div>
				</div>
			</div>
		</li>
		<?php endforeach;?>
	</ul>
</div>