<input 
	type="submit"
    class="btn template-btn-1"
 	data-sending='<?php esc_html_e('Sending Message','narcos') ?>'
	data-sent='<?php esc_html_e('Message Successfully Sent','narcos') ?>'
	value="<?php echo isset($label) && $label !== '' ? $label : 'Send' ?>">