<div class="input-line">
	<textarea data-parsley-errors-container="#results" class="form-input check-value" id="<?php echo esc_attr($name)?>" name="<?php echo esc_attr($name)?>" placeholder="<?php echo esc_attr($placeholder)?>"  <?php if(!empty($required)) echo 'data-parsley-required="true"'; ?> data-parsley-error-message="<?php echo esc_attr($label) . esc_html__(' is equired','narcos');?>"></textarea>
	<span class="label" data-name="<?php echo esc_attr($label)?>"></span>
</div>
