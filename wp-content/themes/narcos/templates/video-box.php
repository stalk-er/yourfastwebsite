<?php 
	$tt_page_id = tt_narcos_get_page_id();
	$tt_video_show = get_post_meta( $tt_page_id, THEME_NAME . '_video_show', true );
	$tt_video_check = get_post_meta( $tt_page_id, THEME_NAME . '_video_check', true );
	$tt_video_bg = get_post_meta( $tt_page_id, THEME_NAME . '_video_bg', true );
	$tt_video_embed = get_post_meta( $tt_page_id, THEME_NAME . '_video_embed', true );

	if($tt_video_check) {
		$tt_page_bg = !empty($tt_video_bg) ? $tt_video_bg['url'] : '';
		$tt_page_embed = $tt_video_embed;
	} elseif(_go('show_video_box')) {
		$tt_page_bg = _go('video_background');
		$tt_page_embed = _go('video_embed');
	}
?>

<?php if(_go('show_video_box'))
	if($tt_video_show):?>
	<!-- Video Box -->
	<div class="video-box" data-parallax-bg="<?php echo esc_attr($tt_page_bg);?>" data-embed='<?php echo esc_attr($tt_page_embed);?>'>
		<div class="box-img-wrapper">
			<div class="box-img">
				<span></span>
			</div>
		</div>

		<div class="video-toggle-wrapper align-center">
			<span class="video-toggle">
				<i class="icon-video-icon"></i>
			</span>
		</div>
	</div>
<?php endif;?>
