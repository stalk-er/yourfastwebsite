<?php $tt_post_id = get_the_ID();
	  $tt_class = !has_post_thumbnail() || empty($tt_audio_meta) ? 'no-image-post' : '';
	  $tt_audio_meta = get_post_meta($tt_post_id , THEME_NAME . '_audio_url', true);
	  $tt_classes = array(esc_attr('blog-post'),$tt_class);
	  ?>

<article <?php post_class($tt_classes);?>>
	<div class="blog-post-cover">
		<?php if(!empty($tt_audio_meta)):?>
		<div class="embed-responsive embed-responsive-16by9">
			<?php echo apply_filters('the_content',$tt_audio_meta); ?>
		</div>
		<?php elseif(has_post_thumbnail()): ?>
		<a href="<?php the_permalink();?>">
            <?php the_post_thumbnail('featured-image');?>
        </a>
		<?php endif; ?>
	</div>
	<?php get_template_part('templates/blog/post' , 'meta');?>
</article>