<?php 
$tt_post_id = get_the_ID();
global $post; 
$tt_attachments = get_children( array(
	'post_parent' => get_the_ID(),
	'post_status' => 'inherit',
	'post_type' => 'attachment',
	'post_mime_type' => 'image',
	'order' => 'ASC',
	'orderby' => 'menu_order ID',
	'numberposts' => 10)
);
$tt_class = !empty($tt_attachments) ? '' : 'no-image-post';
$tt_classes = array(esc_attr('blog-post'),$tt_class);?>

<article <?php post_class($tt_classes);?>>
	<div class="blog-post-cover">
		<?php if(!empty($tt_attachments)): ?>
		<div class="tt-slider blog-post-cover" data-fade="true" data-dots="false" data-speed="650" data-infinite="true">
			<ul class="clean-list slides-list">
				<?php foreach ( $tt_attachments as $tt_thumb_id => $tt_attachment ) : ?>
					<li class="slide">
						<?php echo wp_get_attachment_image($tt_thumb_id, 'featured-image' ); ?>
					</li>
				<?php endforeach; ?>
			</ul>
		</div>
		<?php endif;?>
	</div>

	<?php get_template_part('templates/blog/post' , 'meta');?>
</article>