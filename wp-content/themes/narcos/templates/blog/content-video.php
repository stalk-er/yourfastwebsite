<?php $tt_post_id = get_the_ID();
	  $tt_class = has_post_thumbnail() ? '' : 'no-image-post';
	  $tt_video_meta = get_post_meta($tt_post_id , THEME_NAME . '_video_url', true);
	  $tt_classes = array(esc_attr('blog-post'),esc_attr('video-post'),$tt_class);
	  ?>

<article <?php post_class($tt_classes);?> data-video="<?php echo esc_attr($tt_video_meta);?>">
	<div class="blog-post-cover">
		<?php if(!empty($tt_video_meta)):?>
		<span class="play-btn-toggle video-toggle">
			<i class="icon-play2"></i>
		</span>
		<?php endif;?>

		<?php if(has_post_thumbnail())
	        the_post_thumbnail( 'featured-image' );?>
	</div>

	<?php get_template_part('templates/blog/post' , 'meta');?>
</article>