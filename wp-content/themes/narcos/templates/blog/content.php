<?php $tt_post_id = get_the_ID();
	  $tt_class = has_post_thumbnail() ? '' : 'no-image-post';
	  $tt_classes = array(esc_attr('blog-post'),$tt_class);?>
<article <?php post_class($tt_classes);?>>
	<div class="blog-post-cover">
		<?php if(has_post_thumbnail()):?>
	        <a href="<?php the_permalink();?>">
	            <?php the_post_thumbnail('featured-image');?>
	        </a>
	    <?php endif;?>
	</div>

	<?php get_template_part('templates/blog/post' , 'meta');?>
</article>