<?php
    global $tt_query;
    global $wp_query;
    $tt_paged = 0;
    if(!empty($tt_query))
        $tt_total_pages = $tt_query->max_num_pages;
    else
        $tt_total_pages = $wp_query->max_num_pages;
	$tt_big = 999999999; // need an unlikely integer
	if ( get_query_var('paged') ) 
        { $paged = get_query_var('paged'); }
    elseif ( get_query_var('page') ) 
        { $tt_paged = get_query_var('page'); }
    else { $tt_paged = 1; }
	if ( $tt_total_pages > 1 ) {
		$tt_current_page = $tt_paged;
		$tt_args = array(
			'base' 		   => str_replace( $tt_big, '%#%', esc_url( get_pagenum_link( $tt_big ) ) ),
			'format' 	   => '/page/%#%',
			'total'        => $tt_total_pages,
			'current'      => $tt_current_page,
			'show_all'     => False,
			'end_size'     => 1,
			'mid_size'     => 2,
			'prev_next'    => True,
			'prev_text'    => esc_html_x('Prev','Pagination','narcos'),
			'next_text'    => esc_html_x('Next','Pagination','narcos'),
			'type'         => 'list',
			'add_args'     => False,
			'add_fragment' => ''
		);?>
		<!-- Pagination -->
		<div class="pagination">
			<?php echo paginate_links( $tt_args ); ?>
		</div>
<?php } ?>