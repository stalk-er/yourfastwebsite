<?php 
	$tt_page_id = tt_narcos_get_page_id();
	$tt_blog_bg = get_post_meta( $tt_page_id, THEME_NAME . '_page_background', true );
	
	if(is_single() && has_post_thumbnail($tt_page_id)) {
		$tt_thumb = wp_get_attachment_image_src( get_post_thumbnail_id($tt_page_id), 'large-featured-image');
		$tt_blog_bg = $tt_thumb[0];
	} elseif(is_single() && !has_post_thumbnail($tt_page_id)) {
		$tt_blog_bg = '';
	} elseif($tt_blog_bg) {
		$tt_blog_bg = $tt_blog_bg['url'];
	} elseif(_go('intro_box_bg')) {
		$tt_blog_bg = _go('intro_box_bg');
	} else {
                $tt_blog_bg = '';
        }
?>
<!-- Intro Box -->
<div class="intro-box <?php print is_single() ? esc_attr('single-post ') : esc_attr(''); if(empty($tt_blog_bg)) echo esc_attr('no-image-post');?>" data-parallax-bg="<?php echo esc_attr($tt_blog_bg);?>">
	<div class="box-img-wrapper">
		<div class="box-img">
			<span></span>
		</div>
	</div>

	<div class="intro-box-inner container">
		<div class="box-content align-center">
			<h2 class="page-title">
				<?php if (is_category()) { ?><?php esc_html_e('Category:', 'narcos'); ?> <strong><?php single_cat_title(); ?></strong>
                <?php } elseif( is_tag() ) { ?><?php esc_html_e('Post Tagged with:', 'narcos'); ?> <strong>"<?php single_tag_title(); ?>"</strong>
                <?php } elseif (is_day()) { ?><?php esc_html_e('Archive for: ', 'narcos'); ?> <strong><?php the_time('F jS, Y'); ?></strong>
                <?php } elseif (is_month()) { ?><?php esc_html_e('Archive for: ', 'narcos'); ?> <strong><?php the_time('F, Y'); ?></strong>
                <?php } elseif (is_year()) { ?><?php esc_html_e('Archive for: ', 'narcos'); ?> <strong><?php the_time('Y'); ?></strong>
                <?php } elseif (is_author()) { ?><?php esc_html_e('Author Archives: ', 'narcos'); echo '<strong>'.get_the_author().'</strong>'; ?>  
                <?php } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?><?php esc_html_e('Archives', 'narcos');?>
                <?php } elseif (is_search()) { ?><?php esc_html_e('Results for: ','narcos'); ?><?php echo get_search_query(); ?>
                <?php } elseif (is_home()) { ?><?php esc_html_e('Home','narcos'); ?>
                <?php } else { ?><?php echo get_the_title($tt_page_id);?><?php } ?>
			</h2>
			<?php echo tt_breadcrumbs();?>
		</div>
	</div>

	<!-- Scroll Down Btn -->
	<a href="#" class="scroll-btn">
		<span class="icon"></span>
	</a>

	<!-- Rounded Border Bottom -->
	<div class="rounded-border-bottom">
		<span></span>
	</div>
</div>