<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
if(is_shop() || is_product_category() || is_product_tag()) {
    $sidebar_pos = get_post_meta( get_option( 'woocommerce_shop_page_id' ), THEME_NAME . '_sidebar_position', true );
    $col_class = $sidebar_pos == 'full_width' ? 'col-sm-12 col-md-6' : 'col-sm-12';
} else {
    $col_class = 'col-sm-12 col-md-6';
}

$classes[] = esc_attr('shop-product');
?>
<div class="<?php echo esc_attr($col_class); ?>">
    <div <?php post_class( $classes ); ?>>
        <div class="product-cover">
            <?php
                /**
                 * woocommerce_before_shop_loop_item_title hook
                 *
                 * @hooked woocommerce_show_product_loop_sale_flash - 10
                 * @hooked woocommerce_template_loop_product_thumbnail - 10
                 */
                do_action( 'woocommerce_before_shop_loop_item_title' ); ?>

            <div class="product-action">
                <?php

                    /**
                     * woocommerce_after_shop_loop_item hook
                     *
                     * @hooked woocommerce_template_loop_add_to_cart - 10
                     * @hooked tt_woocommerce_quickview_btn - 20
                     */
                    do_action( 'woocommerce_after_shop_loop_item' );
                ?>
            </div>
        </div>

        <div class="product-body align-center">
             <a href="<?php the_permalink(); ?>">
            <?php
                /**
                 * woocommerce_shop_loop_item_title hook
                 *
                 * @hooked woocommerce_template_loop_product_title - 10
                 */
                do_action( 'woocommerce_shop_loop_item_title' );
                ?>
            </a>

            <?php
                /**
                 * woocommerce_after_shop_loop_item_title hook
                 *
                 * @hooked woocommerce_template_loop_price - 10
                 */
                do_action( 'woocommerce_after_shop_loop_item_title' );
            ?>
        </div>
    </div>
</div>