<?php
/**
 * Review order table
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>

<div class="widget widget_order_totals">
    <h5 class="widget-caption"><?php esc_html_e('Your order','narcos');?></h5>

    <div class="products-box">
        <div class="box-header">
            <span class="header-title"><?php esc_html_e( 'Product', 'narcos' ); ?></span>
            <span class="header-title last"><?php esc_html_e( 'Total', 'narcos' ); ?></span>
        </div>

        <ul class="clean-list products-list">
            <?php
                do_action( 'woocommerce_review_order_before_cart_contents' );

                foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                    $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                    $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

                    if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                        ?>
                            <li class="product <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'order', $cart_item, $cart_item_key ) ); ?>">
                                <p class="product-title">
                                    <?php
                                        if ( ! $_product->is_visible() ) {
                                            echo $_product->get_title();
                                        } else {
                                            printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) );
                                        }
                                    ?>

                                    <?php echo apply_filters( 'woocommerce_checkout_cart_item_quantity', ' <strong class="product-quantity">' . sprintf( '&times; %s', $cart_item['quantity'] ) . '</strong>', $cart_item, $cart_item_key ); ?>
                                </p>

                                <span class="price"><?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); ?></span>
                            </li>
                        <?php
                    }
                }

                do_action( 'woocommerce_review_order_after_cart_contents' );
            ?>   
        </ul>

        <div class="box-footer">
            <span class="title"><?php esc_html_e( 'Cart subtotal', 'narcos' ); ?></span>
            <span class="total-price"><?php wc_cart_totals_subtotal_html(); ?></span>
        </div>
    </div>

    <?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
        <div class="cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
            <span class="label"><?php wc_cart_totals_coupon_label( $coupon ); ?></span>
            <span class="field"><?php wc_cart_totals_coupon_html( $coupon ); ?></span>
        </div>
    <?php endforeach; ?>

    <?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
        <div class="shipping">
            <?php do_action( 'woocommerce_review_order_before_shipping' ); ?>

            <?php wc_cart_totals_shipping_html(); ?>

            <?php do_action( 'woocommerce_review_order_after_shipping' ); ?>
        </div>
    <?php endif; ?>

    <?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
        <div class="fee">
            <span class="label"><?php echo esc_html( $fee->name ); ?></span>
            <span class="content"><?php wc_cart_totals_fee_html( $fee ); ?></span>
        </div>
    <?php endforeach; ?>

    <?php if ( wc_tax_enabled() && 'excl' === WC()->cart->tax_display_cart ) : ?>
        <?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
            <?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : ?>
                <div class="tax-rate tax-rate-<?php echo sanitize_title( $code ); ?>">
                    <span class="label"><?php echo esc_html( $tax->label ); ?></label>
                    <span class="content"><?php echo wp_kses_post( $tax->formatted_amount ); ?></span>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <div class="tax-total">
                <span class="label"><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></span>
                <span class="content"><?php wc_cart_totals_taxes_total_html(); ?></span>
            </tr>
        <?php endif; ?>
    <?php endif; ?>
    
    <?php do_action( 'woocommerce_review_order_before_order_total' ); ?>

        <div class="order-totals">
            <span class="title"><?php esc_html_e( 'Total', 'narcos' ); ?></span>
            <span class="total-price"><?php wc_cart_totals_order_total_html(); ?></span>
        </div>

    <?php do_action( 'woocommerce_review_order_after_order_total' ); ?>


