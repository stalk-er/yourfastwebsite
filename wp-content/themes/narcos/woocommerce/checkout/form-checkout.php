<?php
/**
 * Checkout Form
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$checkout = WC()->checkout();

if ( empty( $_POST ) ) {
    $ship_to_different_address = get_option( 'woocommerce_ship_to_destination' ) === 'shipping' ? 1 : 0;
    $ship_to_different_address = apply_filters( 'woocommerce_ship_to_different_address_checked', $ship_to_different_address );
} else {
    $ship_to_different_address = $checkout->get_value( 'ship_to_different_address' );
}

// filter hook for include new pages inside the payment method
$get_checkout_url = apply_filters( 'woocommerce_get_checkout_url', WC()->cart->get_checkout_url() ); ?>

<div class="section-checkout">
    <?php // If checkout registration is disabled and not logged in, the user cannot checkout
        if ( ! $checkout->enable_signup && ! $checkout->enable_guest_checkout && ! is_user_logged_in() ) {
            echo apply_filters( 'woocommerce_checkout_must_be_logged_in_message', esc_html__( 'You must be logged in to checkout.', 'narcos' ) );
            return;
        }
    ?>

    <div class="row">
        <div class="col-md-14 col-lg-12 col-lg-offset-2">
            <?php wc_print_notices(); ?>

            <!-- Login & Coupon -->
            <div class="row">
                <?php do_action( 'woocommerce_before_checkout_form', $checkout ); ?>
            </div>

        </div>
    </div>

    <form name="checkout" method="post" class="checkout woocommerce-checkout checkout-form main-checkout-form" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">

        <div class="row">
            <div class="col-md-14 col-lg-12 col-lg-offset-2">

                <?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>

                <?php do_action( 'woocommerce_checkout_billing' ); ?>
                <?php do_action( 'woocommerce_checkout_shipping' ); ?>

                <?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>

            </div>

            <div class="col-md-10 col-lg-8 col-lg-offset-2">
                <?php do_action( 'woocommerce_checkout_before_order_review' ); ?>

                <aside class="sidebar">
                    <?php do_action( 'woocommerce_checkout_order_review' ); ?>
                </aside>

                <?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
            </div>
        </div>

    </form>
</div>

<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>