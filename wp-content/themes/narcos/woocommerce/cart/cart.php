<?php
/**
 * Cart Page
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.3.8
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$crosssells = WC()->cart->get_cross_sells(); ?>


<?php wc_print_notices(); ?>
<?php do_action( 'woocommerce_before_cart' ); ?>

<div class="cart-products-box">

    <form action="<?php echo esc_url( WC()->cart->get_cart_url() ); ?>" method="post">

        <?php do_action( 'woocommerce_before_cart_table' ); ?>

        <div class="box-header">
            <div class="row">
                <div class="col-md-12">
                    <span class="box-header-title first"><?php esc_html_e('Product','narcos');?></span>
                </div>

                <div class="col-md-4">
                    <span class="box-header-title"><?php esc_html_e('Price','narcos');?></span>
                </div>

                <div class="col-md-4">
                    <span class="box-header-title"><?php esc_html_e('Quantity','narcos');?></span>
                </div>

                <div class="col-md-4">
                    <span class="box-header-title"><?php esc_html_e('Total','narcos');?></span>
                </div>
            </div>
        </div>

        <div class="box-body">
            <?php do_action( 'woocommerce_before_cart_contents' ); ?>

            <?php
            foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
                $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
                $product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

                if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
                    ?>
                    <div class="cart-product <?php echo esc_attr( apply_filters( 'woocommerce_cart_item_class', 'cart_item', $cart_item, $cart_item_key ) ); ?>">
                        <div class="row">
                            <div class="col-md-12">
                                <?php
                                echo apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
                                    '<a href="%s" class="remove-product" title="%s" data-product_id="%s" data-product_sku="%s"><i class="icon-cross"></i></a>',
                                    esc_url( WC()->cart->get_remove_url( $cart_item_key ) ),
                                    esc_html__( 'Remove this item','narcos' ),
                                    esc_attr( $product_id ),
                                    esc_attr( $_product->get_sku() )
                                ), $cart_item_key );
                                ?>

                                <div class="product-image">
                                    <?php
                                    $thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $cart_item_key );

                                    if ( ! $_product->is_visible() ) {
                                        echo "$thumbnail";
                                    } else {
                                        printf( '<a href="%s">%s</a>', esc_url( $_product->get_permalink( $cart_item ) ), $thumbnail );
                                    }
                                    ?>
                                </div>

                                <h5 class="product-title">
                                    <?php
                                    if ( ! $_product->is_visible() ) {
                                        echo apply_filters( 'woocommerce_cart_item_name', $_product->get_title(), $cart_item, $cart_item_key ) . '&nbsp;';
                                    } else {
                                        echo apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s </a>', esc_url( $_product->get_permalink( $cart_item ) ), $_product->get_title() ), $cart_item, $cart_item_key );
                                    }

                                    // Meta data
                                    echo WC()->cart->get_item_data( $cart_item );

                                    // Backorder notification
                                    if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
                                        echo '<p class="backorder_notification">' . esc_html__( 'Available on backorder','narcos' ) . '</p>';
                                    }
                                    ?>
                                </h5>
                            </div>

                            <div class="col-md-4">
                                <?php echo apply_filters( 'woocommerce_cart_item_price', WC()->cart->get_product_price( $_product ), $cart_item, $cart_item_key ); ?>  
                            </div>

                            <div class="col-md-4">
                                <?php
                                    if ( $_product->is_sold_individually() ) {
                                        $product_quantity = sprintf( '<div class="quantity-block nr-filter"><input class="nr-only" type="text" name="cart[%s][qty]" value="1" readonly/></div>', $cart_item_key );
                                    } else {
                                        $product_quantity = woocommerce_quantity_input( array(
                                            'input_name'  => "cart[{$cart_item_key}][qty]",
                                            'input_value' => $cart_item['quantity'],
                                            'max_value'   => $_product->backorders_allowed() ? '' : $_product->get_stock_quantity(),
                                            'min_value'   => '0'
                                        ), $_product, false );
                                    }

                                    echo apply_filters( 'woocommerce_cart_item_quantity', $product_quantity, $cart_item_key, $cart_item );
                                ?>
                            </div>

                            <div class="col-md-4">
                                <?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ) ?>
                            </div>
                        </div>
                    </div>
                    <?php }
                }

                do_action( 'woocommerce_cart_contents' );
            ?>

            <?php do_action( 'woocommerce_after_cart_contents' ); ?>
        </div>

        <?php do_action( 'woocommerce_after_cart_table' ); ?>

        <div class="coupon-box clearfix">
            <?php if ( WC()->cart->coupons_enabled() ) : ?>
            <div class="coupon-form">
                <input type="text" name="coupon_code" class="form-input check-value" id="coupon_code" value="" placeholder="<?php esc_attr_e( 'Coupon code','narcos' ); ?>" />
                <input type="submit" class="button apply-coupon" name="apply_coupon" value="<?php esc_attr_e( 'Apply Coupon', 'narcos' ); ?>" />
                <?php do_action( 'woocommerce_cart_coupon' ); ?>
            </div>
            <?php endif ?>

            <?php do_action( 'woocommerce_cart_actions' ); ?>
            <input type="submit" class="btn template-btn-1 update-btn" name="update_cart" value="<?php esc_attr_e( 'Update Cart', 'narcos' ); ?>" />
        </div>

        <?php do_action( 'tt_woocommerce_cart_totals' ); ?>

        <?php wp_nonce_field( 'woocommerce-cart' ); ?>
        <?php do_action( 'woocommerce_after_cart_table' ); ?>
    </form>
</div>

<?php do_action( 'woocommerce_proceed_to_checkout' ); ?>

<?php do_action( 'woocommerce_after_cart' ); ?>


