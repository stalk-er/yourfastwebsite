<?php
/**
 * Login Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<div class="login-section">

    <?php wc_print_notices(); ?>

    <?php do_action( 'woocommerce_before_customer_login_form' ); ?>

    <?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

    <div class="row">

        <div class="col-sm-12 col-md-9">

    <?php endif; ?>

            <form class="login-form simple-template-form" method="post">

                <h5 class="form-title align-center"><?php esc_html_e( 'Login', 'narcos' ); ?></h5>

                <?php do_action( 'woocommerce_login_form_start' ); ?>

                <label class="input-line username" for="username">
                    <input type="text" placeholder="<?php esc_html_e('Username','narcos') ?>" class="form-input check-value" name="username" id="username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>" />
                </label>

                <label class="input-line password" for="password">
                    <input type="password" name="password" id="password" placeholder="<?php esc_html_e( 'Enter password', 'narcos' ); ?>" class="form-input check-value" />
                </label>

                <?php do_action( 'woocommerce_login_form' ); ?>

                <?php wp_nonce_field( 'woocommerce-login', 'woocommerce-login-nonce' ); ?>

                <div class="submit">
                    <input type="submit" class="form-login-btn" name="login" value="<?php esc_attr_e( 'Login', 'narcos' ); ?>" />
                </div>

                <label for="rememberme" class="inline">
                    <input name="rememberme" type="checkbox" id="rememberme" value="forever" /> <?php esc_html_e( 'Remember me', 'narcos' ); ?>
                </label>

                <label class="password-recover align-right">
                    <span class="recover-password-toggle"><a href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'narcos' ); ?></a></span>
                </label>

                <?php do_action( 'woocommerce_login_form_end' ); ?>

            </form>

    <?php if ( get_option( 'woocommerce_enable_myaccount_registration' ) === 'yes' ) : ?>

        </div>

        <?php if(_go('account_image')): ?>
            <div class="col-md-6 hidden-sm">
                <div class="banner">
                    <img src="<?php _eo('account_image') ?>" alt="shoes banner" />
                </div>
            </div>
        <?php endif ?>

        <div class="col-sm-12 col-md-9">

            <form class="register-form simple-template-form" method="post">                

                <h5 class="form-title align-center"><?php esc_html_e('Register','narcos') ?></h5>

                <?php do_action( 'woocommerce_register_form_start' ); ?>
                
                <label class="input-line email" for="reg_email">
                    <input type="text" placeholder="<?php esc_html_e('Enter your email','narcos') ?>" class="form-input check-value" name="email" id="reg_email" value="<?php if ( ! empty( $_POST['email'] ) ) echo esc_attr( $_POST['email'] ); ?>"  />
                </label>                

                <?php if ( 'no' === get_option( 'woocommerce_registration_generate_username' ) ) : ?>

                    <label class="input-line username" for="reg_username">
                        <input type="text" placeholder="<?php esc_html_e('Username','narcos') ?>" class="form-input check-value" name="username" id="reg_username" value="<?php if ( ! empty( $_POST['username'] ) ) echo esc_attr( $_POST['username'] ); ?>"  />
                    </label>

                <?php endif ?>

                <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

                    <label class="input-line password" for="reg_password">
                        <input type="password" placeholder="<?php esc_html_e('Enter password','narcos') ?>" class="form-input check-value" name="password" id="reg_password" />
                    </label>

                <?php endif ?>

                <!-- Spam Trap -->
                <div style="<?php echo ( ( is_rtl() ) ? 'right' : 'left' ); ?>: -999em; position: absolute;"><label for="trap"><?php esc_html_e( 'Anti-spam', 'narcos' ); ?></label><input type="text" name="email_2" id="trap" tabindex="-1" /></div>

                <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>

                <div class="submit">
                    <input type="submit" class="form-login-btn register" name="register" value="<?php esc_attr_e( 'Register', 'narcos' ); ?>" />
                </div>

                <?php do_action( 'woocommerce_register_form_end' ); ?>
            </form>

        </div>

    </div>

<?php endif; ?>

<?php do_action( 'woocommerce_after_customer_login_form' ); ?>

</div>
