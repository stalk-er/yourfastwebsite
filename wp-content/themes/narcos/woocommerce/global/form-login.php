<?php
/**
 * Login form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/global/form-login.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( is_user_logged_in() ) {
	return;
}
?>
<form method="post" class="checkout-form login-form login"  <?php if ( $hidden ) echo 'style="display:none;"'; ?>>

	<?php do_action( 'woocommerce_login_form_start' ); ?>

	<?php if ( $message ) echo wpautop( wptexturize( $message ) ); ?>

	<input type="text" class="form-input check-value" name="username" id="username" placeholder="<?php esc_html_e( 'Username or email', 'narcos' ); ?>" />
	<input type="password" class="form-input check-value" name="password" id="password" placeholder="<?php esc_html_e( 'Password', 'narcos' ); ?>" />

	<?php do_action( 'woocommerce_login_form' ); ?>

	<div class="form-action clearfix">
		
		<?php wp_nonce_field( 'woocommerce-login' ); ?>
		<input type="submit" class="button btn template-btn-1" name="login" value="<?php esc_attr_e( 'Login', 'narcos' ); ?>" />
		<input type="hidden" name="redirect" value="<?php echo esc_url( $redirect ) ?>" />

		<label class="checkbox">
			<input class="checkbox-input hidden" name="rememberme" type="checkbox" id="rememberme" value="forever" />
			<span class="label"><?php esc_html_e( 'Remember me', 'narcos' ); ?></span>
		</label>

		<a class="lost-password-btn" href="<?php echo esc_url( wp_lostpassword_url() ); ?>"><?php esc_html_e( 'Lost your password?', 'narcos' ); ?></a>
	</div>

	<?php do_action( 'woocommerce_login_form_end' ); ?>
</form>