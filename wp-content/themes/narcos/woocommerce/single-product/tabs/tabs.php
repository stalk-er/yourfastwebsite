<?php
/**
 * Single Product tabs
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>

<div class="tabed-content checkout-tabs">
	<div class="tabs-header">
		<ul class="clean-list">
			<?php foreach ( $tabs as $key => $tab ) : ?>
			<li data-tab-link="<?php print $key == 'reviews' ? esc_attr('reviews-tab') : esc_attr($key);?>" class="tab-link <?php if($key == 'reviews') echo esc_attr('current');?>"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?></li>
			<?php endforeach;?>
		</ul>
	</div>

	<div class="tabs-body">
		<?php foreach ( $tabs as $key => $tab ) : ?>
		<div id="<?php print $key == 'reviews' ? esc_attr('reviews-tab') : esc_attr($key);?>" class="tab-item <?php if($key == 'reviews') echo esc_attr('current');?>">
			<?php call_user_func( $tab['callback'], $key, $tab ); ?>
		</div>
		<?php endforeach;?>
	</div>
</div>

<?php endif; ?>