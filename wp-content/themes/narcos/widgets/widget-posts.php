<?php
 // Popular Posts
 class popular_posts extends WP_Widget {
 
	function __construct() {
		parent::__construct(
				'widget-popular-posts',
				'['.THEME_PRETTY_NAME.'] Blog Posts',
				array(
					'description' => esc_html__('Show your posts.', 'narcos'),
					'classname' => 'widget_popular_posts',
				)
		);
	}

 
	function widget($args, $instance){
		extract($args);
		$tt_title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		$tt_show_image = isset( $instance['show_image'] ) ? $instance['show_image'] : false;
		$tt_postscount = !empty($instance['number']) ? $instance['number'] : 3;
		$tt_sortby = empty( $instance['sortby'] ) ? 'popular_order' : $instance['sortby'];


	print $before_widget;
	if(!empty($tt_title)){
		print $before_title . $tt_title . $after_title;
	}

	if($tt_sortby == 'popular_order')
		$args = array( 
			'post_type' => 'post',
	        'orderby' => 'meta_value_num',
	        'meta_key' => 'tt_post_views_count',
	        'order' => 'DESC',
			'posts_per_page' => $tt_postscount,
			'ignore_sticky_posts'=>true
		);
	else 
		$args = array( 
		'post_type' => 'post',
        'orderby' => 'date',
        'order' => 'DESC',
		'posts_per_page' => $tt_postscount,
		'ignore_sticky_posts'=>true
	);

	$tt_query_p = new WP_QUERY($args);
	?>
	
	<ul class="clean-list popular-posts">
	<?php while ( $tt_query_p ->have_posts() ) : $tt_query_p ->the_post(); ?>
		<li class="popular-post">
			<?php if ( $tt_show_image ) : ?>
			<div class="cover">
				<a href="<?php the_permalink();?>">
					<?php if(has_post_thumbnail())
						the_post_thumbnail('thumbnail');
					else {?>
						<img src="holder.js/150x150/auto" alt="<?php the_title(); ?>" />
					<?php } ;?>
				</a>
			</div>
			<?php endif;?>

			<h5 class="post-title">
				<a href="<?php the_permalink();?>"><?php the_title();?></a>
			</h5>

			<ul class="post-meta clean-list">
				<li><?php the_time(get_option('date_format'));?></li>
				<li><?php the_author(); ?></li>
			</ul>
		</li>
	<?php endwhile; wp_reset_postdata(); ?>
	</ul>

	<?php print $after_widget;
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_image'] = isset( $new_instance['show_image'] ) ? (bool) $new_instance['show_image'] : false;

		if ( in_array( $new_instance['sortby'], array( 'popular_order', 'date_order') ) ) {
			$instance['sortby'] = $new_instance['sortby'];
		} else {
			$instance['sortby'] = 'popular_order';
		}

		$instance['exclude'] = sanitize_text_field( $new_instance['exclude'] );

		return $instance;
	}
 
	function form( $instance ) {
		$tt_title   = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$tt_number  = isset( $instance['number'] ) ? absint( $instance['number'] ) : 3;
		$tt_sortby  = isset( $instance['sortby'] ) ? $instance['sortby'] : 'popular_order';
		$tt_show_image = isset( $instance['show_image'] ) ? (bool) $instance['show_image'] : false;
	?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:' , 'narcos'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $tt_title; ?>" /></p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'sortby' ) ); ?>"><?php esc_html_e( 'Sort by:', 'narcos' ); ?></label>
			<select name="<?php echo esc_attr( $this->get_field_name( 'sortby' ) ); ?>" id="<?php echo esc_attr( $this->get_field_id( 'sortby' ) ); ?>" class="widefat">
				<option value="popular_order"<?php selected( $tt_sortby, 'popular_order' ); ?>><?php esc_html_e('Popularity', 'narcos'); ?></option>
				<option value="date_order"<?php selected( $tt_sortby, 'date_order' ); ?>><?php esc_html_e('Date', 'narcos'); ?></option>
			</select>
		</p>

		<p><input class="checkbox" type="checkbox" <?php checked( $tt_show_image ); ?> id="<?php echo $this->get_field_id( 'show_image' ); ?>" name="<?php echo $this->get_field_name( 'show_image' ); ?>" />
		<label for="<?php echo $this->get_field_id( 'show_image' ); ?>"><?php esc_html_e( 'Display thumbnails?', 'narcos' ); ?></label></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php esc_html_e( 'Number of posts to show:' ,'narcos'); ?></label>
		<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $tt_number; ?>" size="3" /></p>
	<?php
	}
}
 
add_action('widgets_init', create_function('', 'return register_widget("popular_posts");')); ?>