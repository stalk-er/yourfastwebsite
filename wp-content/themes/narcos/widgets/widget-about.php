<?php

class Tesla_about_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
				'tesla_about_widget',
				'['.THEME_PRETTY_NAME.'] About',
				array(
					'description' => esc_html__('Displays website info', 'narcos'),
					'classname' => 'widget_about',
				)
		);
	}

	function widget($args, $instance) {
		extract($args);
		$tt_description  = apply_filters( 'widget_text', $instance['description'], $instance );
		$tt_email = apply_filters( 'widget_text', $instance['email'], $instance );
		$tt_phone = apply_filters( 'widget_text', $instance['phone'], $instance );

		print $before_widget;
		?>

			<a href="<?php echo home_url(); ?>" style="<?php _estyle_changer('logo_text') ?>" >
		        <?php if(_go('logo_text')): ?>
		            <?php _eo('logo_text') ?>
		        <?php elseif(_go('logo_image')): ?>
		            <img src="<?php _eo('logo_image') ?>" alt="<?php echo THEME_PRETTY_NAME ?>">
		        <?php else: ?>
		            <?php echo THEME_PRETTY_NAME; ?>
		        <?php endif; ?>
		    </a>

			<p><?php print $tt_description;?></p>

			<ul class="clean-list contact-info">
				<?php 
				if(!empty($tt_email))
					echo '<li class="mail"><a href="mailto:'.$tt_email.'">'.$tt_email.'</a></li>';
				if(!empty($tt_phone))
					echo '<li class="phone">'.$tt_phone.'</li>';
				?>
			</ul>

		<?php
		print $after_widget;
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['description'] = $new_instance['description'];
		$instance['email'] = $new_instance['email'];
		$instance['phone'] = $new_instance['phone'];

		return $instance;
	}

	function form($instance) {
		$tt_description = isset( $instance['description'] ) ? $instance['description'] : '';
		$tt_email = isset( $instance['email'] ) ? $instance['email'] : '';
		$tt_phone = isset( $instance['phone'] ) ? $instance['phone'] : '';
		?>
		<p>
			<label><?php _e('Description:','narcos'); ?><input class="widefat" name="<?php echo esc_attr($this->get_field_name('description')); ?>" type="text" value="<?php echo esc_attr($tt_description); ?>" /></label>
		</p>
		<p>
			<label><?php _e('Email:','narcos'); ?><input class="widefat" name="<?php echo esc_attr($this->get_field_name('email')); ?>" type="text" value="<?php echo esc_attr($tt_email); ?>" /></label>
		</p>
		<p>
			<label><?php _e('Phone:','narcos'); ?><input class="widefat" name="<?php echo esc_attr($this->get_field_name('phone')); ?>" type="text" value="<?php echo esc_attr($tt_phone); ?>" /></label>
		</p>
		<?php
	}
}

add_action('widgets_init', create_function('', 'return register_widget("Tesla_about_widget");'));