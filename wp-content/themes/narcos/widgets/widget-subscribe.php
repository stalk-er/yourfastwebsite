<?php
 // Subscribe Box
 class tt_subscribe_box extends WP_Widget {
 
	function __construct() {
		parent::__construct(
				'widget_subscribe',
				'['.THEME_PRETTY_NAME.'] Subscribe Box',
				array(
					'description' => esc_html__('Subscribe Box Widget.', 'narcos'),
					'classname' => 'widget_subscribe',
				)
		);
	}

 
	function widget($args, $instance){
		extract($args);
		$tt_title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );

	print $before_widget;
	if(!empty($tt_title)){
		print $before_title . $tt_title . $after_title;
	}

	?>
	<form class="subscribe-form" id="newsletter" method="post" data-tt-subscription>
		<div class="result_container"></div>
		<input type="text" name="email" class="form-input check-value" placeholder="<?php esc_html_e('Your email address','narcos');?>"  data-tt-subscription-required data-tt-subscription-type="email" />
		<button class="btn template-btn-1 form-submit"><?php esc_html_e('Subscribe','narcos');?></button>
	</form>
	<?php print $after_widget;
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		return $instance;
	}
 
	function form( $instance ) {
		$tt_title = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
	?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php esc_html_e( 'Title:' , 'narcos'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $tt_title; ?>" /></p>
	<?php
	}
}
 
add_action('widgets_init', create_function('', 'return register_widget("tt_subscribe_box");')); ?>