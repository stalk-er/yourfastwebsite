<div class="container-fluid">
	<div class="row">
		<div class="col-lg-20 col-lg-offset-2">
			<div class="row">
				<?php if( is_active_sidebar( 'footer_1' )):?>
					<div class="col-md-7"> 
					<?php dynamic_sidebar('footer_1'); ?>
					</div>
				<?php endif;?>

				<?php if( is_active_sidebar( 'footer_2' )):?>
					<div class="col-md-3"> 
					<?php dynamic_sidebar('footer_2'); ?>
					</div>
				<?php endif;?>

				<?php if( is_active_sidebar( 'footer_3' )):?>
					<div class="col-md-8"> 
					<?php dynamic_sidebar('footer_3'); ?>
					</div>
				<?php endif;?>

				<?php if( is_active_sidebar( 'footer_4' )):?>
					<div class="col-md-6"> 
					<?php dynamic_sidebar('footer_4'); ?>
					</div>
				<?php endif;?>
			</div>
		</div>
	</div>
</div>