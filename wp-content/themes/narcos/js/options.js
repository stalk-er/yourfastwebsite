(function ($) {
	'use strict';

	// Template Helper Function
	$.fn.hasAttr = function(attribute) {
		var obj = this;

		if (obj.attr(attribute) !== undefined) {
			return true;
		} else {
			return false;
		}
	};

	function checkVisibility (object) {
		var el = object[0].getBoundingClientRect(),
			wHeight = $(window).height(),
			scrl =  wHeight - (el.bottom - el.height),
			condition = wHeight + el.height;

		if (scrl > 0  && scrl < condition) {
			return true;
		} else {
			return false;
		}
	};

	// Scroll Events
	var keys = {37: 1, 38: 1, 39: 1, 40: 1};
	function preventDefault(e) {
		e = e || window.event;
		if (e.preventDefault)
			e.preventDefault();
		e.returnValue = false;  
	};
	function preventDefaultForScrollKeys(e) {
	    if (keys[e.keyCode]) {
	        preventDefault(e);
	        return false;
	    }
	};
	function disableScroll() {
		if (window.addEventListener) window.addEventListener('DOMMouseScroll', preventDefault, false);
		window.onwheel = preventDefault; // modern standard
		window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
		window.ontouchmove  = preventDefault; // mobile
		document.onkeydown  = preventDefaultForScrollKeys;
	};
	function enableScroll() {
		if (window.removeEventListener) window.removeEventListener('DOMMouseScroll', preventDefault, false);
		window.onmousewheel = document.onmousewheel = null; 
		window.onwheel = null; 
		window.ontouchmove = null;  
		document.onkeydown = null;
	};
	function carousel_define(item, obj) {
		item.slick({
			focusOnSelect: true,
			speed: obj.hasAttr('data-speed') ? obj.data('speed') : 600,
			slidesToShow: obj.hasAttr('data-items-desktop') ? obj.data('items-desktop') : 4,
			arrows: obj.hasAttr('data-arrows') ? obj.data('arrows') : true,
			dots: obj.hasAttr('data-dots') ? obj.data('dots') : true,
			infinite: obj.hasAttr('data-infinite') ? obj.data('infinite') : false,
			slidesToScroll: obj.hasAttr('data-items-to-slide') ? obj.data('items-to-slide') : 1,
			initialSlide: obj.hasAttr('data-start') ? obj.data('start') : 0,
			asNavFor: obj.hasAttr('data-as-nav-for') ? obj.data('as-nav-for') : '',
			centerMode: obj.hasAttr('data-center-mode') ? obj.data('center-mode') : '',
			vertical: obj.hasAttr('data-vertical') ? obj.data('vertical') : false,
			responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: obj.hasAttr('data-items-small-desktop') ? obj.data('items-small-desktop') : 3,
                        slidesToScroll: obj.hasAttr('data-items-small-desktop') ? obj.data('items-small-desktop') : 3
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: obj.hasAttr('data-items-tablet') ? obj.data('items-tablet') : 2,
                        slidesToScroll: obj.hasAttr('data-items-tablet') ? obj.data('items-tablet') : 2
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: obj.hasAttr('data-items-phone') ? obj.data('items-phone') : 2,
                        slidesToScroll: obj.hasAttr('data-items-phone') ? obj.data('items-phone') : 2
                    }
                }
            ]
		});
	}

	function slider_define(item, obj) {
		item.slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			focusOnSelect: true,
			fade: obj.hasAttr('data-fade') ? obj.data('fade') : false,
			dots: obj.hasAttr('data-dots') ? obj.data('dots') : true,
			speed: obj.hasAttr('data-speed') ? obj.data('speed') : 500,
			arrows: obj.hasAttr('data-arrows') ? obj.data('arrows') : true,
			infinite: obj.hasAttr('data-infinite') ? obj.data('infinite') : false,
			initialSlide: obj.hasAttr('data-start') ? obj.data('start') : 0,
			asNavFor: obj.hasAttr('data-as-nav-for') ? obj.data('as-nav-for') : ''					
		});
	}

	var Base64 = {
	    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	    encode: function(e) {
	        var t = "";
	        var n, r, i, s, o, u, a;
	        var f = 0;
	        e = Base64._utf8_encode(e);
	        while (f < e.length) {
	            n = e.charCodeAt(f++);
	            r = e.charCodeAt(f++);
	            i = e.charCodeAt(f++);
	            s = n >> 2;
	            o = (n & 3) << 4 | r >> 4;
	            u = (r & 15) << 2 | i >> 6;
	            a = i & 63;
	            if (isNaN(r)) {
	                u = a = 64
	            } else if (isNaN(i)) {
	                a = 64
	            }
	            t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
	        }
	        return t
	    },
	    decode: function(e) {
	        var t = "";
	        var n, r, i;
	        var s, o, u, a;
	        var f = 0;
	        e = e.replace(/[^A-Za-z0-9+/=]/g, "");
	        while (f < e.length) {
	            s = this._keyStr.indexOf(e.charAt(f++));
	            o = this._keyStr.indexOf(e.charAt(f++));
	            u = this._keyStr.indexOf(e.charAt(f++));
	            a = this._keyStr.indexOf(e.charAt(f++));
	            n = s << 2 | o >> 4;
	            r = (o & 15) << 4 | u >> 2;
	            i = (u & 3) << 6 | a;
	            t = t + String.fromCharCode(n);
	            if (u != 64) {
	                t = t + String.fromCharCode(r)
	            }
	            if (a != 64) {
	                t = t + String.fromCharCode(i)
	            }
	        }
	        t = Base64._utf8_decode(t);
	        return t
	    },
	    _utf8_encode: function(e) {
	        e = e.replace(/rn/g, "n");
	        var t = "";
	        for (var n = 0; n < e.length; n++) {
	            var r = e.charCodeAt(n);
	            if (r < 128) {
	                t += String.fromCharCode(r)
	            } else if (r > 127 && r < 2048) {
	                t += String.fromCharCode(r >> 6 | 192);
	                t += String.fromCharCode(r & 63 | 128)
	            } else {
	                t += String.fromCharCode(r >> 12 | 224);
	                t += String.fromCharCode(r >> 6 & 63 | 128);
	                t += String.fromCharCode(r & 63 | 128)
	            }
	        }
	        return t
	    },
	    _utf8_decode: function(e) {
	        var t = "";
	        var n = 0;
	        var r = 0;
	        var c1 = 0;
	        var c2 = 0;
	        while (n < e.length) {
	            r = e.charCodeAt(n);
	            if (r < 128) {
	                t += String.fromCharCode(r);
	                n++
	            } else if (r > 191 && r < 224) {
	                c2 = e.charCodeAt(n + 1);
	                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
	                n += 2
	            } else {
	                c2 = e.charCodeAt(n + 1);
	                c3 = e.charCodeAt(n + 2);
	                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
	                n += 3
	            }
	        }
	        return t
	    }
	}

	var teslaThemes = {
		init: function () {
			this.checkInputsForValue();
			this.nrOnlyInputs();
			this.nrFilters();
			this.parallaxInit();
			this.slickInit();
			this.customSelectBoxes();
			this.tabsInit();
			this.toggles();
			this.accordionInit();
			this.isotopeInit();
			this.googleMaps();
			this.showCases();
			this.foldingContent();
			this.animatedCounters();
			this.cartSettings();
			this.googleMapInit();
			this.introBoxFix();
		},

		// Template Custom Functions
		checkInputsForValue: function () {
			$(document).on('focusout', '.check-value', function () {
				var text_val = $(this).val();
				if (text_val === "" || text_val.replace(/^\s+|\s+$/g, '') === "") {
					$(this).removeClass('has-value');
				} else {
					$(this).addClass('has-value');
				}
			});
		},

		nrOnlyInputs: function () {
			$('.nr-only').keypress(function (e) {
				if (e.which !== 8 && e.which !== 0 && (e.which < 48 || e.which > 57)) {
					return false;
				}
			});
		},

		nrFilters: function () {
			$('.nr-filter').each(function () {
				var input = $(this).find('input'),
					substract = $(this).find('.substract'),
					add = $(this).find('.add');

				add.on('click', function () {
					input.val(Math.max(1, parseInt(input.val()) + 1));
				});

				substract.on('click', function () {
					input.val(Math.max(1, parseInt(input.val()) - 1));
				});
			});
		},

		parallaxInit: function () {
			var container = jQuery('[data-parallax-bg]');

			if (container.length) {
				container.each(function(index) {
					var boxImg = container.eq(index),
						boxImgData = boxImg.data('parallax-bg'),
						parallaxBox = boxImg.find('.box-img > span');

					parallaxBox.css({
						'background-image': 'url("' + boxImgData + '")'
					});

					function scrollEffect() {
						var elCont = container[index],
							el = elCont.getBoundingClientRect(),
							wHeight = jQuery(window).height(),
							scrl =  wHeight-(el.bottom - el.height),
							scrollBox = boxImg.find('.box-img'),
							condition = wHeight+el.height,
							progressCoef = scrl/condition;

						if( scrl > 0  && scrl < condition) {
							scrollBox.css({
								transform: 'translateY('+(progressCoef * 100)+'px)'
							});
						}
					}

					scrollEffect();

					jQuery(window).scroll(function() {
						scrollEffect();
					});
				});
			}

			// Main Scroll Event
			$(window).on('scroll', function () {
				// Show Visible Elements
				$('.check-screen-visibility').each(function () {
					var obj = $(this);

					if (obj.visible()) {
						setTimeout(function () {
							obj.addClass('visible');
						}, 250);
					}
				});
			});
		},

		slickInit: function () {
			// Get All Carousels from the page
			var carousel = $('.tt-carousel');

			// Get All Sliders from the page
			var slider = $('.tt-slider');

			// Init Carousels
			carousel.each(function () {
				var obj = $(this),
					items = obj.find('.carousel-items');

				carousel_define(items, obj);					
			});

			// Init Sliders
			slider.each(function () {
				var obj = $(this),
					items = obj.find('.slides-list');

				slider_define(items, obj);
			});
		},

		customSelectBoxes: function () {
			$('.fake-select-box').each(function () {
				var selectBox = $(this);

				selectBox.find('input').on('click', function () {
					$('.fake-select-box').not(selectBox).removeClass('open');
					selectBox.toggleClass('open');
				});

				selectBox.find('.select-options .option').on('click', function () {
					selectBox.find('input').attr('value', $(this).text());
					selectBox.removeClass('open');
				});

				selectBox.on('click', function (e) {
					e.stopPropagation();
				});

				$(document).on('click', function () {
					selectBox.removeClass('open');
				});
			});
		},

		tabsInit: function () {
			var tabs = $('.tabed-content');

			tabs.each(function () {
				var tab = $(this),
					option = tab.find('.tabs-header .tab-link'),
					content = tab.find('.tab-item');

				option.on('click', function () {
					var obj = $(this);

					if (!obj.hasClass('current')) {
						option.removeClass('current');
						obj.addClass('current');

						if (tabs.hasClass('gallery-tabs')) {
							tab.addClass('switching');

							setTimeout(function () {
								content.removeClass('current');
								$('#' + obj.data('tab-link')).addClass('current');

								tabs.removeClass('switching');
							}, 1795);
						} else {
							content.removeClass('current');
							$('#' + obj.data('tab-link')).addClass('current');
						}
					}
				});
			});
		},

		toggles: function () {
			$('.rating-box').on('click', function () {
				$('.rating-box').removeClass('active');
				$(this).addClass('active');
			});

			$('.close-popup-btn').on('click', function () {
				$('html').removeClass('popup-visible quick-view-popup-visible');
			});

			// Video Popup
			$('.blog-post .video-toggle').on('click', function () {
				var video_url = $(this).parents('.blog-post').data('video');
				$('.video-popup .responsive-media').html('<iframe src="'+video_url+'" allowfullscreen=""></iframe>')
				$('html').addClass('video-popup-visible');
				return false;
			});

			$('.video-box .video-toggle').on('click', function () {
				var video_embed = decodeURIComponent(Base64.decode($(this).parents('.video-box').data('embed')));
				$('.video-popup .responsive-media').html(video_embed);
				$('html').addClass('video-popup-visible');
				return false;
			});

			$('.video-popup .media-wrapper').on('click', function (e) {
				e.stopPropagation();
			});

			$('html, .close-video-popup-toggle').on('click', function () {
				$('html').removeClass('video-popup-visible');
				var media = $('.media-wrapper');
				media.html(media.html());
			});

			// Share Block Toggle
			$('.share-block .block-toggle').on('click', function () {
				$('.share-block').toggleClass('options-visible');
			});

			// Shop Cart
			$(document).on('click', '.shopping-cart-wrapper .cart-toggle', function(){
			    //$('.shopping-cart-wrapper').toggleClass('active');

			    if ($('.shopping-cart-wrapper').hasClass('active')) {
			    	$('.shopping-cart-wrapper').removeClass('active')
			    } else {
			    	$('.shopping-cart-wrapper').addClass('active')
			    }

				return false;
			});

			$('.cart-items-wrapper').on('click', function (e) {
				e.stopPropagation();
			});

			$(document).on('click', function () {
				$('.shopping-cart-wrapper').removeClass('active');
			});

			// Search form toggle
			$('.search-toggle').on('click', function () {
				$('.search-form-wrapper').addClass('active');
			});

			$('.close-search-form-wrapper').on('click', function () {
				$('.search-form-wrapper').removeClass('active');
			});

			// Main Slider Toggle
			$('.main-slider .custom-slider-nav .slides-toggle').on('click', function () {
				$('.main-slider .custom-slider-nav').toggleClass('active');

				return false;
			});

			$('.main-slider .custom-slider-nav').on('click', function (e) {
				e.stopPropagation();
			});

			$(document).on('click', function () {
				$('.main-slider .custom-slider-nav').removeClass('active');
			});

			// Checkout Forms
			$('.checkout-heading-block').each(function () {
				var trigger = $(this).find('.heading span'),
					obj = $(this);

				trigger.one('click', function () {
					obj.find('.checkout-form').velocity('slideDown', {duration: 250});
				});
			});

			// Scroll Down Btn
			$('.scroll-btn').on('click', function (e) {
				e.preventDefault();
				$('body').velocity('scroll', {
					offset: $('.intro-box').length ? $('.intro-box').height() : $('.main-slider').length ? $('.main-slider').height() : $('.index-slider').height() + $('.main-header').height() + 40,
					duration: 600
				});
			});

			// Blog Posts Fix
			$('.blog-post-preview').eq(0).imagesLoaded(function () {
				if ($(window).width() > 992) {
					$('.blog-post-preview').eq(1).height($('.blog-post-preview').eq(0).height());
				} else {
					$('.blog-post-preview').eq(1).height('auto');
				}
			});

			$(window).on('resize', function () {
				if ($(window).width() > 992) {
					$('.blog-post-preview').eq(1).height($('.blog-post-preview').eq(0).height());
				} else {
					$('.blog-post-preview').eq(1).height('auto');
				}
			});

			// Menu Toggle - Style 2
			$('.stack-menu-toggle').on('click', function () {
				if (!$('html').hasClass('stack-nav-visible')) {

					if ($(window).scrollTop() !== 0) {
						$('html').velocity('scroll', {
							duration: 100,
							complete: function () {
								setTimeout(function () {
									$('html').addClass('stack-nav-visible perspective overflow-stack-nav');
									disableScroll();
									$(window).trigger('resize');
								}, 150);
							}
						});
					} else {
						$('html').addClass('stack-nav-visible perspective overflow-stack-nav');
						disableScroll();
						$(window).trigger('resize');
					}
				} else {
					enableScroll();
					$('html').removeClass('stack-nav-visible');

					setTimeout(function () {
						$('html').removeClass('perspective overflow-stack-nav');
					}, 450);
				}
			});

			$('.close-stack-nav').on('click', function () {
				enableScroll();
				$('html').removeClass('stack-nav-visible');

				setTimeout(function () {
					$('html').removeClass('overflow-stack-nav');
					$(window).resize();
				}, 450);
			});

			$('.page-wrapper').on('click', function (e) {
				if ($('html').hasClass('stack-nav-visible')) {
					if (!$(e.target).parent().is('.stack-menu-toggle') && !$(e.target).is('.stack-menu-toggle')) {
						enableScroll();
						$('html').removeClass('stack-nav-visible');

						setTimeout(function () {
							$('html').removeClass('overflow-stack-nav');
							$(window).resize();
						}, 450);
					}
				}
			});

			$(window).on('scroll', function () {
				if ($('.stack-nav-wrapper').length || $('.main-slider').length || $('.full-page-intro').length) {
					if ($(window).scrollTop() === 0) {
						$('html').addClass('perspective');
					} else {
						$('html').removeClass('perspective');
					}
				}

				if ($('.page-wrapper').hasClass('sticky-header')) {
					if ($(window).scrollTop() > $('.main-header').outerHeight(true) / 2) {
						$('.main-header').addClass('fixed');
					} else {
						$('.main-header').removeClass('fixed');
					}
				}
			});

			// Demo Blocks 
			if ($('.demo-block').length) {
				if ($(window).width() < 992) {
					$('.demo-block').eq($('.demo-block').length - 1).css({
						'margin-bottom': '95px'
					});
				} else {
					$('.demo-block').eq($('.demo-block').length - 1).css({
						'margin-bottom': '95px'
					});
					$('.demo-block').eq($('.demo-block').length - 2).css({
						'margin-bottom': '95px'
					});
				}
			}

			// Mobile Nav
			$('.mobile-navigation-toggle').on('click', function () {
				$('body').toggleClass('mobile-navigation-visible');

				return false;
			});

			$('.main-header nav > ul').on('click', function (e) {
				if ($(window).width() < 992) {
					e.stopPropagation();
				}
			});

			$(document).on('click', function () {
				$('body').removeClass('mobile-navigation-visible');				
			});

			$('.main-header nav li.menu-item-has-children > a').on('click', function (e) {
				var obj = $(this);

				if ($(window).width() < 992) {
					e.preventDefault();

					obj.parent().toggleClass('open');
					obj.next().slideToggle(225);
				}
			});

			if( $( 'header' ).hasClass( 'logo-center' ) ) {
				var logo = $( 'header .brand-holder' ),
				 	menu = $( 'header .desktop-only-nav > ul > li' ),
				 	count = menu.length % 2 === 0 ? Math.round( menu.length / 2 ) : Math.round( menu.length / 2 ) - 1;

				 	$('header .desktop-only-nav > ul > li').slice( 0, count ).wrapAll( '<ul></ul>' );
				 	$('header .desktop-only-nav > ul > li').wrapAll( '<ul></ul>' );

					$('header .desktop-only-nav > ul > ul').unwrap();

					logo.insertAfter( $('header .desktop-only-nav > ul').eq( 0 ) ).wrap( '<span class="brand-holder"></span>' );
			}
		},

		accordionInit: function () {
			var accordion = $('.accordion-group');

			accordion.each(function () {
				var accordion = $(this).find('.accordion-box');

				accordion.each(function () {
					var obj = $(this),
						header = $(this).find('.box-header h4'),
						body = $(this).find('.box-body');

					header.on('click', function () {
						if (obj.hasClass('open')) {
							body.velocity('slideUp', {
								duration: 150,
								complete: function () {
									obj.removeClass('open');
								}
							});
						} else {
							obj.addClass('open');
							body.velocity('slideDown', {duration: 195});
						}
					});
				});
			});
		},

		isotopeInit: function () {
			var isotopeContainer = $('.isotope-container'),
				defaultSelection = isotopeContainer.data('default-selection');

			// Isotope Init
			isotopeContainer.imagesLoaded(function () {
				isotopeContainer.isotope({
					filter: defaultSelection,
					itemSelector: '.isotope-item'
				});
			});

			// Isotope Filters
			$('.isotope-filters a').on('click', function () {
				$('.isotope-filters .current').removeClass('current');
				$(this).addClass('current');

				var selector = $(this).attr('data-filter');
					isotopeContainer.isotope({
						filter: selector,
						animationOptions: {
						    duration: 945,
						    easing: 'linear',
						    queue: false
						}
					});
				return false;
			});
		},

		googleMaps: function () {
			// Describe Google Maps Init Function 
			function initialize_contact_map (customOptions) {
                var mapOptions = {
                        center: new google.maps.LatLng(customOptions.map_center.lat, customOptions.map_center.lon),
                        zoom: parseInt(customOptions.zoom),
                        scrollwheel: false,
                        disableDefaultUI: true,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        styles: [{ stylers: [{saturation: -100}]}]
                    };
                var contact_map = new google.maps.Map($('#map-canvas')[0], mapOptions),
                    marker = new google.maps.Marker({
                        map: contact_map,
                        position: new google.maps.LatLng(customOptions.marker_coord.lat, customOptions.marker_coord.lon),
                        animation: google.maps.Animation.DROP,
                        icon: customOptions.marker,
                    });
            }
            
            if ($('#map-canvas').length) {
            	var customOptions = $('#map-canvas').data('options');
                google.maps.event.addDomListener(window, 'load', initialize_contact_map(customOptions));
            }
		},

		showCases: function () {
			$(window).on('scroll', function () {
				// MacBook Showcase
				var macBookShowCaseContainer = $('.macbook-showcase');

				if (macBookShowCaseContainer.length) {
					var showCaseContent = macBookShowCaseContainer.find('.inner-content img');

					if (checkVisibility(macBookShowCaseContainer)) {
						var translateValue = $(window).scrollTop() - macBookShowCaseContainer.offset().top + $(window).height();

						showCaseContent.css({
							'transform': 'translateY(' + -translateValue + 'px)'
						});
					}
				}

				// Main Blog Posts Showcase
				var blogPostWrapper = $('.blog-post-preview-wrapper');

				if (blogPostWrapper.length && $(window).width() > 992) {
					if (checkVisibility(blogPostWrapper)) {
						var offset = (Math.min(0, $(window).scrollTop() - $('.section-blog').offset().top + $(window).height() - 850)).toFixed();

					    blogPostWrapper.eq(0).css({
					    	'transform': 'translate('+ offset +'px, '+ Math.abs(offset * 0.65) +'px)'
					    });

					    blogPostWrapper.eq(1).css({
					    	'transform': 'translateY('+ -(offset * 0.35) +'px)'
					    });

					    blogPostWrapper.eq(2).css({
					    	'transform': 'translate('+ Math.abs(offset) +'px, '+ Math.abs(offset * 0.65) +'px)'
					    });
					}
				}

				// Lazy Loading
				var lazyLoadItem = $('.lazy-load');

				if (lazyLoadItem.length) {
					lazyLoadItem.each(function () {
						var obj = $(this);

						if (checkVisibility(obj))
							obj.addClass('show');
					});
				}
			});
		},

		foldingContent: function () {
			var foldingContainer = $('.folding-content'),
				foldingItem = $('.folding-item-text');

			foldingItem.find('> a').on('click', function (e) {
				e.preventDefault();

				if ($(window).width() < 992) {
					foldingContainer.velocity('scroll', {
						duration: 400,
						offset: -30,
						complete: function () {
							foldingContainer.addClass('content-open');
						}
					});
				} else {
					foldingContainer.addClass('content-open');
				}
			});

			$('.close-content-toggle').on('click', function () {
				foldingContainer.removeClass('content-open');
			});
		},

		animatedCounters: function () {
			function animateCounterBoxes () {
				$('.counter-box').each(function () {
				 	var obj = $(this);

				 	if (checkVisibility(obj) && obj.attr('data-state') === "0") {
				 		obj.attr('data-state', '1');

				 		var animationTime = 2500;

			 			$({
					  		Counter: 0
					  	}).animate({
				  			Counter: parseInt(obj.data('counter-value'), 10)
					  	}, {
					    	duration: animationTime,
					    	easing: 'swing',
					    	step: function (now) {
					      		obj.find('.value-container').text(Math.ceil(now));
					    	}
					  	});
				 	}
				});
			};

			animateCounterBoxes();

			$(window).on('scroll', function () {
				animateCounterBoxes();
			});
		},

		cartSettings: function () {
			$('.shop-product').find('.quick-view-btn').on('click', function (e){
				e.preventDefault();

				var productId = $(this).data('id');

					jQuery.ajax({
		                url: ajaxurl,
		                type: 'POST',
		                data: { action: 'load_product_popup', product_id : productId },
		                success: function (result) {
		                    $('.quick-view-box').find('.target').html(result);
		                   	
		                    $('.quick-view-box').find('.target').imagesLoaded(function () {
		                    	carousel_define($('.tt-carousel-popup').find('.carousel-items'), $('.tt-carousel-popup'));
			                   	slider_define($('.tt-slider-popup').find('.slides-list'), $('.tt-slider-popup'));				

			                    $('html').addClass('popup-visible quick-view-popup-visible');
		                    });
		                },
		                error: function (xhr, status, error) {
		                    console.log(xhr,status,error);
		                }
		            });
			});
		},

		googleMapInit: function () {
			function initialize_one() {
		        var mapCanvas = document.getElementsByClassName('map-canvas');
		        var Options = mapCanvas[0].getAttribute('data-map-settings');
		        var Pin = mapCanvas[0].getAttribute('data-pin');
		        var defaultSettings = JSON.parse(atob(Options));
		        var map;

		        var mapOptions = {
		            center: defaultSettings.location,
		            zoom: defaultSettings.zoom,
		            styles: [{ stylers: [{saturation: -100}]}],
		            scrollwheel: false,
		            mapTypeId: google.maps.MapTypeId.ROADMAP
		        };

		        map = new google.maps.Map(mapCanvas[0], mapOptions);
		        var markers = [];
		        if(defaultSettings.markers) {
		            jQuery.each(defaultSettings.markers, function (i, val) {
		                var pos = new google.maps.LatLng(val.lat, val.lng);
		                markers[i] = new google.maps.Marker({
		                    position: pos,
		                    map: map,
		                    icon: Pin,
		                    animation: google.maps.Animation.DROP,
		                });
		            });
		        }
		    }

		    if(jQuery('.map-canvas').length) {
		        google.maps.event.addDomListener(window, 'load', initialize_one);
		    }
		},

		introBoxFix: function () {
			var sectionBg = $('.intro-box').next().css('backgroundColor');
			switch (sectionBg) {
				case 'transparent' : {} break;
				case 'rgba(0, 0, 0, 0)' : {} break;
				default: {
					$('.intro-box').find('.rounded-border-bottom').css({
						'color': sectionBg
					});
				}
			};
		}
	};

	$(document).ready(function(){
		teslaThemes.init();

		setTimeout(function () {
			$('html').addClass('dom-ready');
		}, 320);
	});
}(jQuery));