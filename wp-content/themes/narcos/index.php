<?php get_header(); ?>	

<?php
/**
 * Index Page
 */
$tt_page_id = tesla_get_page_id();
$tt_sidebar = get_post_meta( $tt_page_id, THEME_NAME . '_sidebar_position', true );
$tt_rev_slider = get_post_meta( $tt_page_id, THEME_NAME . '_rev_slider_alias', true );
$tt_sidebar = $tt_sidebar ? $tt_sidebar : 'right';
?>

<?php if(empty($tt_rev_slider))
	get_template_part('templates/intro','box');
else
	echo do_shortcode($tt_rev_slider);
?>

<!-- Blog Section -->
<section class="section section-blog no-margin">
	<div class="row">
		<?php if($tt_sidebar == "left"): ?>
		<div class="col-lg-8 col-lg-offset-2 col-md-9">
			<?php get_sidebar();?>
		</div>
		<?php endif;?>

		<div class="<?php print $tt_sidebar == "full_width" ? esc_attr('col-lg-12 col-lg-offset-6 col-md-18 col-md-offset-3') : esc_attr('col-lg-12 col-lg-offset-2 col-md-15');?>">
			<?php if (have_posts()): ?>	
				<div class="blog-list">		
					<?php while(have_posts()): the_post(); 
						get_template_part('templates/blog/content',get_post_format( ));
					endwhile; ?>
				</div>
				<?php get_template_part('templates/nav','main'); ?>
			<?php else:?>
				<h3 class="no-posts-found"><?php esc_html_e('No Posts Found','narcos');?>
			<?php endif; ?>
		</div>

		<?php if($tt_sidebar == "right"): ?>
		<div class="col-lg-8 col-lg-offset-2 col-md-9">
			<?php get_sidebar();?>
		</div>
		<?php endif;?>
	</div>
</section>
<?php get_footer();?>