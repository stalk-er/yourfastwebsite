<?php get_header();
/**
 * Single post page
 */

$tt_page_id = tt_narcos_get_page_id();
$tt_blog_page_id = get_option( 'page_for_posts' );
$tt_sidebar_option = get_post_meta( $tt_page_id, THEME_NAME . '_sidebar_position', true );

switch ($tt_sidebar_option) {
	case 'as_blog':
		$tt_s_id = $tt_blog_page_id;	
		break;
	case 'full_width':
		$tt_s_id = $tt_page_id;
		break;
	case 'right':
		$tt_s_id = $tt_page_id;
		break;
	case 'left':
		$tt_s_id = $tt_page_id;
}

if(!empty($tt_s_id))
	$tt_sidebar = get_post_meta( $tt_s_id, THEME_NAME . '_sidebar_position', true );
	$tt_sidebar = empty($tt_sidebar) ? 'right' : $tt_sidebar;
?>

<?php get_template_part('templates/intro','box'); ?>

<?php while ( have_posts() ) : the_post(); $tt_author_id = get_the_author_meta( 'ID' ); ?>
	<!-- Single Post Page Heading -->
	<div class="single-post-page-heading">
		<ul class="clean-list post-meta">
			<li><a href="<?php echo get_author_posts_url( $tt_author_id ); ?>"><?php the_author(); ?></a></li>
			<li><?php the_time(get_option('date_format'));?></li>
			<li><?php the_category();?></li>
		</ul>
	</div>

	<!-- Blog Section -->
	<section class="section section-blog no-margin">
		<div class="row">
			<?php if($tt_sidebar == "left"): ?>
			<div class="col-lg-8 col-lg-offset-2 col-md-9">
				<?php get_sidebar();?>
			</div>
			<?php endif;?>

			<div class="<?php print $tt_sidebar == "full_width" ? esc_attr('col-lg-12 col-lg-offset-6 col-md-18 col-md-offset-3') : esc_attr('col-lg-12 col-lg-offset-2 col-md-15');?>">
				<div class="single-post-content">
					<?php the_content();?>
					<div class="post-navigation">
			            <?php wp_link_pages(array(
			                'next_or_number'   => 'number',
			                'nextpagelink'     => esc_html__( 'Next page','narcos' ),
			                'previouspagelink' => esc_html__( 'Previous page','narcos' ),
			                'pagelink'         => '%',
			                'echo' => 1
			            )); ?>
			        </div>
				</div>
			</div>

			<?php if($tt_sidebar == "right"): ?>
			<div class="col-lg-8 col-lg-offset-2 col-md-9">
				<?php get_sidebar();?>
			</div>
			<?php endif;?>
		</div>
	</section>

	<!-- Blog Post Footer -->
	<div class="blog-post-footer">
		<div class="row">
			<div class="col-lg-12 col-lg-offset-2 col-md-15 col-sm-12">
				<?php tt_narcos_share(); ?>
			</div>

			<div class="col-lg-8 col-lg-offset-2 col-md-9 col-sm-12">
				<div class="post_tagcloud">
					<?php the_tags();?>
				</div>
			</div>
		</div>
	</div>

	<?php comments_template();?>

<?php tt_set_post_views(get_the_ID()); endwhile; ?>
<?php get_footer();?>