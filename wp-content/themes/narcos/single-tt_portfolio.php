<?php get_header();
/**
 * Single Portfolio Page
 */

$tt_page_id = tt_narcos_get_page_id();
$tt_sidebar = get_post_meta( $tt_page_id, THEME_NAME . '_sidebar_position', true );
$tt_background = get_post_meta( $tt_page_id, THEME_NAME . '_page_background', true ); $tt_background = !empty($tt_background) ? 'data-parallax-bg="'.$tt_background['url'].'"' : '';
$tt_sidebar = empty($tt_sidebar) ? 'right' : $tt_sidebar;
?>

<?php while ( have_posts() ) : the_post(); 
$tt_terms = get_the_terms( get_the_ID(),'tt_portfolio_tax'); ?>
	<!-- Intro Box -->
	<div class="intro-box style-2 <?php print empty($tt_background) ? esc_attr('no-portfolio-bg') : esc_attr('');?>" <?php print $tt_background;?>>
		<div class="box-img-wrapper">
			<div class="box-img">
				<span></span>
			</div>
		</div>

		<div class="intro-box-inner">
			<div class="box-content">
				<div class="project-info">
					<h2 class="project-title"><?php the_title();?></h2>
					<p class="project-meta"><span><?php the_time(get_option('date_format'));?> | <?php if(!is_wp_error($tt_terms)) { foreach($tt_terms as $term) print $term->name; } ?></p>
				</div>
				<?php echo tt_breadcrumbs();?>
			</div>
		</div>
	</div>

	<!-- Blog Section -->
	<section class="section single-project-view <?php print $tt_sidebar == 'full_width' ? esc_attr('') : esc_attr('section-blog no-margin');?>">
		<div class="row">
			<?php if($tt_sidebar == "left"): ?>
			<div class="col-lg-8 col-lg-offset-2 col-md-9">
				<?php get_sidebar();?>
			</div>
			<?php endif;?>

			<div class="<?php print $tt_sidebar == "full_width" ? esc_attr('col-lg-16 col-lg-offset-4 col-md-18 col-md-offset-3') : esc_attr('col-lg-12 col-lg-offset-2 col-md-15');?>">
				<div class="project-cover">
					<?php the_post_thumbnail();?>
				</div>


				<div class="project-body">
					<?php the_content();?>
					<div class="post-navigation">
			            <?php wp_link_pages(array(
			                'next_or_number'   => 'number',
			                'nextpagelink'     => esc_html__( 'Next page','narcos' ),
			                'previouspagelink' => esc_html__( 'Previous page','narcos' ),
			                'pagelink'         => '%',
			                'echo' => 1
			            )); ?>
			        </div>
				</div>

				<div class="project-footer">
					<div class="share-block-wrapper align-center">
						<?php tt_narcos_share(); ?>
					</div>

					<div class="row">
						<div class="col-sm-8 col-xs-12">
							<div class="nav-item prev">
								<a href="<?php echo get_permalink(get_adjacent_post(false,'',true)); ?>">
									<span class="heading"><?php esc_html_e('Prev item','narcos');?></span>
								</a>
							</div>
						</div>

						<div class="col-sm-8 col-xs-12 col-sm-offset-8">
							<div class="nav-item next">
								<a href="<?php echo get_permalink(get_adjacent_post(false,'',false)); ?>">
									<span class="heading"><?php esc_html_e('Next item','narcos');?></span>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<?php if($tt_sidebar == "right"): ?>
			<div class="col-lg-8 col-lg-offset-2 col-md-9">
				<?php get_sidebar();?>
			</div>
			<?php endif;?>
		</div>
	</section>

	<?php comments_template();?>

<?php endwhile; ?>
<?php get_footer();?>